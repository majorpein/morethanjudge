//
//  MTJPlayer.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 13/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import Foundation

class MTJPlayer: Equatable {
    var number: Int
    var score: Int = 0
    var isActive: Bool = false
    
    init(_ number: Int, isActive: Bool = false) {
        self.isActive = isActive
        self.number = number
    }
    
    static func == (left: MTJPlayer, right: MTJPlayer) -> Bool {
        return left.number == right.number
    }
}
