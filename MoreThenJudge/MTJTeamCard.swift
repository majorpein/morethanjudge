//
//  MTJTeamCard.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 10/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import Foundation

class MTJTeamCard {
    var name: String = NSLocalizedString("DefaultTeamName", comment: "")
    
    init(name: String) {
        self.name = name
    }
    
    var players: [MTJPlayerCard]?
    
    var startPlayers: [MTJPlayerCard]? {
        if self.players != nil {
            var startPlayers = [MTJPlayerCard]()
            for player in self.players! {
                if player.isSelected == true {
                    startPlayers.append(player)
                }
            }
            return startPlayers
        }
        return nil
    }
    
    @discardableResult func addPlayer(player: MTJPlayerCard) -> Int? {
        if self.canUseNumber(player.number) {
            if self.players != nil {
                self.players!.append(player)
            } else {
                self.players = [player]
            }
        }
        return self.players?.index(of: player)
    }
    
    func insertPlayer(_ player: MTJPlayerCard, at index: Int) {
        if self.canUseNumber(player.number) {
            if self.players != nil {
                self.players!.insert(player, at: index)
            }
        }
    }
    
    func removePlayer(at index: Int) {
        self.players?.remove(at: index)
    }
    
    func movePlayer(from fromIndex: Int, to toIndex: Int) {
        if self.players != nil {
            let player = self.players![fromIndex]
            self.removePlayer(at: fromIndex)
            self.insertPlayer(player, at: toIndex)
        }
    }
    
    func playerCanBeSelected() -> Bool {
        guard self.players != nil else {
            return false
        }
        return self.startPlayers!.count < MTJBasketballSettingsManager.basketballManager.startPlayersCount
    }
    
    func playerIsSelected(at index: Int) -> Bool {
        guard self.players != nil else {
            return false
        }
        return self.players![index].isSelected
    }
    
    func playersName(with number: Int) -> String? {
        for player in self.players! {
            if player.number == number {
                return player.name
            }
        }
        return nil
    }
    
    func playersName(at index: Int) -> String? {
        return self.players?[index].name
    }
    
    func playersNumber(at index: Int) -> Int? {
        return self.players?[index].number
    }
    
    func canUseNumber(_ number: Int) -> Bool {
        if self.players != nil {
            for player in self.players! {
                if player.number == number {
                    return false
                }
            }
        }
        return true
    }
    
    func selectPlayer(at index: Int, isSelected: Bool) {
        self.players?[index].isSelected = isSelected && self.playerCanBeSelected()
    }
    
    func isReady() -> Bool {
        guard self.players != nil else {
            return false
        }
        return !self.name.isEmpty && self.startPlayers!.count == MTJBasketballSettingsManager.basketballManager.startPlayersCount
    }
    
    func playersNeeded() -> Int {
        return MTJBasketballSettingsManager.basketballManager.startPlayersCount - (self.startPlayers != nil ? self.startPlayers!.count : 0)
    }
}
