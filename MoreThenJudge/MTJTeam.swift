//
//  MTJTeam.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 13/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import Foundation

class MTJTeam {
    var players: [MTJPlayer]
    
    init(_ players: [MTJPlayer]) {
        self.players = players
        self.sortPlayers()
    }
    
    var score: Int {
        var score = 0
        for player in self.players {
            score += player.score
        }
        return score
    }
    
    func scored(at index: Int, scored points: Int) {
        self.players[index].score += points
    }
    
    func substitutePlayer(at index: Int, with newIndex: Int) {
        if self.players[index].isActive == false || self.players[newIndex].isActive == true {
            return
        }
        self.players[index].isActive = false
        self.players[newIndex].isActive = true
        self.sortPlayers()
    }
    
    func substitutePlayer(with number: Int, with newNumber: Int) {
        let firstIndex = self.indexOfPlayer(with: number)
        let secondIndex = self.indexOfPlayer(with: newNumber)
        if firstIndex != nil && secondIndex != nil {
            self.substitutePlayer(at: firstIndex!, with: secondIndex!)
        }
    }
    
    func sortPlayers() {
        self.players.sort(by: { (p1: MTJPlayer, p2: MTJPlayer) -> Bool in
            if p1.isActive != p2.isActive {
                return p1.isActive == true
            } else {
                return p1.number < p2.number
            }
        })
    }
    
    func player(with number: Int) -> MTJPlayer? {
        for player in self.players {
            if player.number == number {
                return player
            }
        }
        return nil
    }
    
    func indexOfPlayer(with number: Int) -> Int? {
        if let player = self.player(with: number) {
            return self.players.index(of: player)
        }
        return nil
    }
    
    func indexOfActivePlayer(with number: Int) -> Int? {
        var i = 0
        for player in self.players where player.isActive == true {
            if player.number == number {
                return i
            }
            i += 1
        }
        return nil
    }
    
    func activePlayerNumber(at index: Int) -> Int? {
        var i = 0
        for player in self.players where player.isActive == true {
            if i == index {
                return player.number
            }
            i += 1
        }
        return nil
    }
    
    func playerScored(_ score: Int, with number: Int) {
        if let player = self.player(with: number) {
            player.score += score
        }
    }
    
    func playersScore(with number: Int) -> Int {
        for player in self.players {
            if player.number == number {
                return player.score
            }
        }
        return 0
    }
}
