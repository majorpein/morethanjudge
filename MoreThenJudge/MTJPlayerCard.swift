//
//  MTJPlayerCard.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 10/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import Foundation

class MTJPlayerCard: Equatable {
    var name: String? = ""
    var number: Int = 0
    var isSelected: Bool = false
    
    weak var team: MTJTeamCard?
    
    init(number: Int, name: String?, team: MTJTeamCard?) {
        self.name = name
        self.number = number
        self.team = team
    }
    
    static func == (left: MTJPlayerCard, right: MTJPlayerCard) -> Bool {
        return left.number == right.number && left.name == right.name
    }
}
