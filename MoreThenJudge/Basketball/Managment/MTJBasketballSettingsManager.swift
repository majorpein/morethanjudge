//
//  MTJBasketballManager.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 26/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

let maxPlayerNumber = 99
let minPlayerNumber = 1

enum MTJBballGameOvertimeType: Int {
    case noOvertime = 0, madeGoal, fixTime
    
    static let descriptions = [
        noOvertime: NSLocalizedString("OvertimeDescriptionNoOvertime", comment: ""),
        madeGoal: NSLocalizedString("OvertimeDescriptionMadeGoal", comment: ""),
        fixTime: NSLocalizedString("OvertimeDescriptionFixTime", comment: "")
    ]
    
    static let titles = [
        noOvertime: "",
        madeGoal: NSLocalizedString("OvertimeTitleMadeGoal", comment: ""),
        fixTime: NSLocalizedString("OvertimeTitleFixTime", comment: ""),
        ]
    
    static let countDescriptions = [
        noOvertime: "",
        madeGoal: "OvertimeCountDescriptionMadeGoal",
        fixTime: "OvertimeCountDescriptionFixTime"
    ]
}

var maxGoalsCount           = 9

var minGoalsCount           = 1

class MTJBasketballSettingsManager: MTJSettingsManager {
    
    override init() {
        super.init()
        name = NSLocalizedString("Basketball", comment: "")
        iconName = "basketball.png"
    }

    static let basketballManager = MTJBasketballSettingsManager()
    
    override func setDefaultValues() {
        super.setDefaultValues()
        if let defaultAtackSecondsCount = UserDefaults.standard.object(forKey: "atackSecondsCount") as! Int? {
            self.atackSecondsCount = defaultAtackSecondsCount
        }
        
        if let defaultOvertimeType = UserDefaults.standard.object(forKey: "overtimeType") as! Int? {
            if let validOvertimeType = MTJBballGameOvertimeType(rawValue: defaultOvertimeType) {
                self.overtimeType = validOvertimeType
            }
        }
        if let defaultOvertimeGoalsCount = UserDefaults.standard.object(forKey: "overtimeGoalsCount") as! Int? {
            self.overtimeGoalsCount = defaultOvertimeGoalsCount
        }
        if let defaultOvertimeMinutesCount = UserDefaults.standard.object(forKey: "overtimeMinutesCount") as! Int? {
            self.overtimeMinutesCount = defaultOvertimeMinutesCount
        }
        
        //DEBUG
        
        self.homeTeamCard = MTJTeamCard(name: "LAKERS")
        
        let kb = MTJPlayerCard(number: 24, name: "Kobe Bryant", team: self.homeTeamCard)
        let kbIndex = self.homeTeamCard.addPlayer(player: kb)
        if kbIndex != nil {
            self.homeTeamCard.selectPlayer(at: kbIndex!, isSelected: true)
        }
        
        let magic = MTJPlayerCard(number: 32, name: "Magic Johnson", team: self.homeTeamCard)
        let magicIndex = self.homeTeamCard.addPlayer(player: magic)
        if magicIndex != nil {
            self.homeTeamCard.selectPlayer(at: magicIndex!, isSelected: true)
        }
        
        let shaq = MTJPlayerCard(number: 34, name: "Shaquille O'Neal", team: self.homeTeamCard)
        let shaqIndex = self.homeTeamCard.addPlayer(player: shaq)
        if shaqIndex != nil {
            self.homeTeamCard.selectPlayer(at: shaqIndex!, isSelected: true)
        }
        
        let jw = MTJPlayerCard(number: 44, name: "Jerry West", team: self.homeTeamCard)
        let jwIndex = self.homeTeamCard.addPlayer(player: jw)
        if jwIndex != nil {
            self.homeTeamCard.selectPlayer(at: jwIndex!, isSelected: true)
        }
        
        self.awayTeamCard = MTJTeamCard(name: "SPURS")
        
        let td = MTJPlayerCard(number: 21, name: "Tim Duncan", team: self.awayTeamCard)
        let tdIndex = self.awayTeamCard.addPlayer(player: td)
        if tdIndex != nil {
            self.awayTeamCard.selectPlayer(at: tdIndex!, isSelected: true)
        }
        
        let tp = MTJPlayerCard(number: 9, name: "Tony Parker", team: self.awayTeamCard)
        let tpIndex = self.awayTeamCard.addPlayer(player: tp)
        if tpIndex != nil {
            self.awayTeamCard.selectPlayer(at: tpIndex!, isSelected: true)
        }
        
        let claw = MTJPlayerCard(number: 2, name: "Kawhi Leonard", team: self.awayTeamCard)
        let clawIndex = self.awayTeamCard.addPlayer(player: claw)
        if clawIndex != nil {
            self.awayTeamCard.selectPlayer(at: clawIndex!, isSelected: true)
        }
        
        let manu = MTJPlayerCard(number: 20, name: "Manu Ginobili", team: self.awayTeamCard)
        let manuIndex = self.awayTeamCard.addPlayer(player: manu)
        if manuIndex != nil {
            self.awayTeamCard.selectPlayer(at: manuIndex!, isSelected: true)
        }
    }
    
    // MARK: - Game time periods settings
    
    var atackSecondsCount: Int = 7 {
        didSet {
            if atackSecondsCount > maxAtackSecondsCount {
                atackSecondsCount = maxAtackSecondsCount
            }
            if atackSecondsCount < minAtackSecondsCount {
                atackSecondsCount = minAtackSecondsCount
            }
            UserDefaults.standard.set(atackSecondsCount, forKey:"atackSecondsCount")
        }
    }
    
    var canDecreaseAtackSecondsCount: Bool {
        return self.atackSecondsCount != minAtackSecondsCount
    }
    
    var canEncreaseAtackSecondsCount: Bool {
        return self.atackSecondsCount != maxAtackSecondsCount
    }
    
    // MARK: - Overtime settings
    
    var overtimeType: MTJBballGameOvertimeType = .noOvertime {
        didSet {
            UserDefaults.standard.set(overtimeType.rawValue, forKey:"overtimeType")
        }
    }
    
    var hasOvertime: Bool {
        return self.overtimeType != .noOvertime
    }
    
    var overtimeGoalsCount: Int = 3 {
        didSet {
            if overtimeGoalsCount > maxGoalsCount {
                overtimeGoalsCount = maxGoalsCount
            }
            if overtimeGoalsCount < minGoalsCount {
                overtimeGoalsCount = minGoalsCount
            }
            UserDefaults.standard.set(overtimeGoalsCount, forKey:"overtimeGoalsCount")
        }
    }
    
    var overtimeMinutesCount: Int = 5 {
        didSet {
            if overtimeMinutesCount > maxMinutesCount {
                overtimeMinutesCount = maxMinutesCount
            }
            if overtimeMinutesCount < minMinutesCount {
                overtimeMinutesCount = minMinutesCount
            }
            UserDefaults.standard.set(overtimeMinutesCount, forKey:"overtimeMinutesCount")
        }
    }
    
    var overtimeCount: Int {
        get {
            if self.overtimeType == .madeGoal {
                return self.overtimeGoalsCount
            } else if self.overtimeType == .fixTime {
                return self.overtimeMinutesCount
            } else {
                return 0
            }
        }
        set(newCount) {
            if self.overtimeType == .madeGoal {
                self.overtimeGoalsCount = newCount
            } else if self.overtimeType == .fixTime {
                self.overtimeMinutesCount = newCount
            }
        }
    }
    
    func selectedOvertimeDescription() -> String? {
        return MTJBballGameOvertimeType.descriptions[self.overtimeType]
    }
    
    func selectedOvertimeTitle() -> String? {
        return MTJBballGameOvertimeType.titles[overtimeType]
    }
    
    func selectedOvertimeCountDescription() -> String? {
        
        if let keyString = MTJBballGameOvertimeType.countDescriptions[self.overtimeType] {
            return keyString.plural(for: self.overtimeCount)
        }
        return nil
    }
    
    var minOvertimeCount: Int {
        if (self.overtimeType == .madeGoal) {
            return minGoalsCount
        } else {
            return minMinutesCount
        }
    }
    
    var maxOvertimeCount: Int {
        if (self.overtimeType == .madeGoal) {
            return maxGoalsCount
        } else {
            return maxMinutesCount
        }
    }
    
    var canDecreaseOvertimeCount: Bool {
        return self.overtimeCount != self.minOvertimeCount
    }
    
    var canEncreaseOvertimeCount: Bool {
        return self.overtimeCount != self.maxOvertimeCount
    }
    
    // MARK: game process
    
    static var basketballEngine: MTJBasketballGameEngine?
    
    static func initBasketballEngine() {
        
        self.basketballEngine = MTJBasketballGameEngine(homeTeamCard: self.basketballManager.homeTeamCard, awayTeamCard: self.basketballManager.awayTeamCard)
        //MTJBasketballSettingsManager.basketballEngine = MTJBasketballGameEngine(homeTeamCard: self.basketballManager.homeTeamCard, awayTeamCard: self.basketballManager.awayTeamCard)
    }
}
