//
//  MTJBasketballGameEngine.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 13/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import Foundation

class MTJBasketballGameEngine : MTJGameEngine {
    
    private var currentAtackTimeLeft: Int = 0 {
        didSet {
            if currentAtackTimeLeft <= 0 {
                self.endOfAtack()
            }
        }
    }
    
    var currentAtackSecondsLeft: Int {
        return self.currentAtackTimeLeft / ticksInSecond + (self.currentAtackTimeLeft % ticksInSecond > 0 ? 1 : 0)
    }
    
    func decreaseCurrentAtackTimeLeft() {
        self.currentAtackTimeLeft -= 1
    }
    
    func resetCurrentAtackTimeLeft() {
        self.currentAtackTimeLeft = basketballManager.atackSecondsCount * ticksInSecond
    }
    
    var atackClockAnimationFromValue: Double {
        let fullAtackTime: Double = Double(basketballManager.atackSecondsCount * ticksInSecond)
        let atackTimeLeft: Double = Double(self.currentAtackTimeLeft)
        
        return atackTimeLeft / fullAtackTime
    }
    
    var atackClockAnimationToValue: Double {
        let fullAtackTime: Double = Double(basketballManager.atackSecondsCount * ticksInSecond)
        let atackTimeLeft: Double = Double(self.currentAtackTimeLeft)
        
        return atackTimeLeft == 0.0 ? 0.0 : (atackTimeLeft - 1) / fullAtackTime
    }
    
    override func startGame() {
        self.resetCurrentAtackTimeLeft()
        super.startGame()
    }
    
    func endOfAtack() {
        self.resetCurrentAtackTimeLeft()
    }
    
    override func endOfPeriod() {
        
        super.endOfPeriod()
        self.resetCurrentAtackTimeLeft()
    }
    
    override func endOfGame() {
        self.resetCurrentAtackTimeLeft()
        super.endOfGame()
    }
    
    override func timerTick() {
        self.decreaseCurrentAtackTimeLeft()
        super.timerTick()
    }
}
