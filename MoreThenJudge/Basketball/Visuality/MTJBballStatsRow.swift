//
//  MTJBballStatsRow.swift
//  MoreThenJudge
//
//  Created by Alexandro on 26/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballStatsRow: UIView {

    let nameLabel: UILabel
    let scoreLabel: UILabel
    let number: Int
    let border: UIView
    
    override var frame: CGRect {
        didSet {
            self.updateFrames()
        }
    }

    init(number:Int, name: String?, score: Int, frame: CGRect = CGRect.zero) {
        
        self.number = number
        
        self.nameLabel = UILabel()
        self.nameLabel.textColor = UIColor.white
        self.nameLabel.font = UIFont.systemFont(ofSize: 14.0)
        self.nameLabel.text = name != nil ? "\(number) \(name!)" : String(number)
        
        self.scoreLabel = UILabel()
        self.scoreLabel.textColor = score == 0 ? UIColor(white: 1.0, alpha: 0.5) : UIColor.white
        self.scoreLabel.font = UIFont.systemFont(ofSize: 14.0)
        self.scoreLabel.text = String(score)
        
        self.border = UIView()
        self.border.backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        
        super.init(frame: frame)
        
        self.addSubview(self.scoreLabel)
        self.addSubview(self.nameLabel)
        self.addSubview(self.border)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFrames() {
        self.scoreLabel.sizeToFit()
        self.scoreLabel.center = CGPoint(x: self.frame.size.width / 2.0, y: self.frame.size.height / 2.0)
        self.scoreLabel.frame.origin.x = self.frame.size.width - self.scoreLabel.frame.size.width - 24.0
        
        self.nameLabel.sizeToFit()
        self.nameLabel.frame.size.width = min(self.nameLabel.frame.size.width, 220.0)
        self.nameLabel.center = CGPoint(x: self.frame.size.width / 2.0, y: self.frame.size.height / 2.0)
        self.nameLabel.frame.origin.x = 24.0
        
        self.border.frame = CGRect.init(x: 0.0, y: self.frame.size.height, width: self.frame.size.width, height: 1.0)
    }
}
