//
//  MTJDeselectableSegmentedControl.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 27/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJDeselectableSegmentedControl: UISegmentedControl {

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let previousSelectedSegmentIndex = self.selectedSegmentIndex
        super.touchesEnded(touches, with: event)
        if previousSelectedSegmentIndex == self.selectedSegmentIndex {
            if let touch = touches.first {
                let touchLocation = touch.location(in: self)
                if bounds.contains(touchLocation) {
                    self.sendActions(for: .valueChanged)
                }
            }
        }
    }
    
    func setBordersVisible(_ visible: Bool) {
        self.setBackgroundImage(visible ? nil : imageWithColor(color: self.backgroundColor!), for: .normal,   barMetrics: .default)
        self.setBackgroundImage(visible ? nil : imageWithColor(color: self.tintColor!),       for: .selected, barMetrics: .default)
        
        self.setDividerImage(visible ? nil : imageWithColor(color: .clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
