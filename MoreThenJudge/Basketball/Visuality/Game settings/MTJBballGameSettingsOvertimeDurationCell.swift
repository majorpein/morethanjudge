//
//  MTJBballGameSettingsOvertimeDurationCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 30/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballGameSettingsOvertimeDurationCell: UITableViewCell, MTJBballGameSettingsCellProtocol {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countDescriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var lessButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update() {
        let basketballManager = MTJBasketballSettingsManager.basketballManager
        
        self.countLabel.text = String(basketballManager.overtimeCount)
        self.titleLabel.text = basketballManager.selectedOvertimeTitle()
        self.countDescriptionLabel.text = basketballManager.selectedOvertimeCountDescription()
        self.lessButton.isEnabled = basketballManager.canDecreaseOvertimeCount
        self.moreButton.isEnabled = basketballManager.canEncreaseOvertimeCount
    }
}
