//
//  MTJBballGameSettingsTimeTypeCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 25/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballGameSettingsTimeTypeCell: UITableViewCell, MTJBballGameSettingsCellProtocol {

    @IBOutlet weak var timeTypeControl: UISegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let normalAttributes = [
            NSForegroundColorAttributeName: UIColor(white: 1.0, alpha: 0.19),
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)
        ]
        
        let selectedAttributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)
        ]
        
        timeTypeControl.setTitleTextAttributes(normalAttributes   as [NSObject : AnyObject], for: .normal)
        timeTypeControl.setTitleTextAttributes(selectedAttributes as [NSObject : AnyObject], for: .selected)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update() {
        self.timeTypeControl.selectedSegmentIndex = MTJBasketballSettingsManager.basketballManager.timeType.rawValue
    }

}
