//
//  MTJBballGameSettingsTableViewController.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 23/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

let systemMediumFontName = "HelveticaNeue-Medium"

struct MTJBballSettingsSectionInfo {
    let identificator: String
    let rowHeight: Double
}

protocol MTJBballGameSettingsCellProtocol {
    func update()
}

enum MTJBballGameSettingsSection: Int {
    case timeType = 0, timePeriods, overtime
    
    static var sections: [MTJBballGameSettingsSection : [MTJBballSettingsSectionInfo]] {
        var sections = [
            timeType:   [MTJBballSettingsSectionInfo(identificator:"MTJBballGameSettingsSectionTimeTypeCell", rowHeight:192.0)],
            timePeriods:[MTJBballSettingsSectionInfo(identificator:"MTJBballGameSettingsSectionTimePeriodsCell", rowHeight:365.0)],
            overtime:   [
                MTJBballSettingsSectionInfo(identificator:"MTJBballGameSettingsSectionOvertimeTypeCell", rowHeight:222.0),
                MTJBballSettingsSectionInfo(identificator:"MTJBballGameSettingsSectionNextCell", rowHeight:86.0)
            ]
        ]
        if (MTJBasketballSettingsManager.basketballManager.hasOvertime) {
            sections[.overtime]?.insert(MTJBballSettingsSectionInfo(identificator:"MTJBballGameSettingsSectionOvertimeDurationCell", rowHeight:106.0), at: overtimeDurationCellIndexPath.row)
        }
        return sections
    }
    
    static let overtimeDurationCellIndexPath = IndexPath(row: 1, section: overtime.rawValue)
    
    static func identificator (forSection section: Int, row: Int) -> String? {
        if let section = MTJBballGameSettingsSection(rawValue: section) {
            if let sectionArray = sections[section] {
                return sectionArray[row].identificator
            }
        }
        return nil
    }
    
    static func rowHeight (forSection section: Int, row: Int) -> Double? {
        if let section = MTJBballGameSettingsSection(rawValue: section) {
            if let sectionArray = sections[section] {
                return sectionArray[row].rowHeight
            }
        }
        return nil
    }
    
    static func rowsCount (forSection section: Int) -> Int? {
        if let section = MTJBballGameSettingsSection(rawValue: section) {
            if let sectionArray = sections[section] {
                return sectionArray.count
            }
        }
        return nil
    }
    
    static var count: Int {
        return sections.count
    }
}

class MTJBballGameSettingsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MTJSportInitialProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    let basketballManager = MTJBasketballSettingsManager.basketballManager
    
    let overtimeTypeMap = [
        UISegmentedControlNoSegment : MTJBballGameOvertimeType.noOvertime,
        0                           : MTJBballGameOvertimeType.madeGoal,
        1                           : MTJBballGameOvertimeType.fixTime
    ]
    
    func name() -> String {
        return basketballManager.name
    }
    
    func iconName() -> String {
        return basketballManager.iconName
    }
    
    var timeTypeCell: MTJBballGameSettingsTimeTypeCell? {
        if let cell: MTJBballGameSettingsTimeTypeCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: MTJBballGameSettingsSection.timeType.rawValue)) as? MTJBballGameSettingsTimeTypeCell {
            return cell
        }
        return nil
    }
    
    var timePeriodsCell: MTJBballGameSettingsTimePeriodsCell? {
        if let cell: MTJBballGameSettingsTimePeriodsCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: MTJBballGameSettingsSection.timePeriods.rawValue)) as? MTJBballGameSettingsTimePeriodsCell {
            return cell
        }
        return nil
    }
    
    var overtimeTypeCell: MTJBballGameSettingsOvertimeCell? {
        if let cell: MTJBballGameSettingsOvertimeCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: MTJBballGameSettingsSection.overtime.rawValue)) as? MTJBballGameSettingsOvertimeCell {
            return cell
        }
        return nil
    }
    
    var overtimeDurationCell: MTJBballGameSettingsOvertimeDurationCell? {
        if let cell: MTJBballGameSettingsOvertimeDurationCell = self.tableView.cellForRow(at: IndexPath(row: 1, section: MTJBballGameSettingsSection.overtime.rawValue)) as? MTJBballGameSettingsOvertimeDurationCell {
            return cell
        }
        return nil
    }
    
    var nextCell: MTJBballGameSettingsNextCell? {
        if let cell: MTJBballGameSettingsNextCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: MTJBballGameSettingsSection.overtime.rawValue)) as? MTJBballGameSettingsNextCell {
            return cell
        }
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Needed to hide empty rows
        tableView.tableFooterView = UIView()
        
        basketballManager.gameSettingsStarted()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return MTJBballGameSettingsSection.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MTJBballGameSettingsSection.rowsCount(forSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let rowHeight = MTJBballGameSettingsSection.rowHeight(forSection: indexPath.section, row: indexPath.row) {
            return CGFloat(rowHeight)
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let identificator = MTJBballGameSettingsSection.identificator(forSection: indexPath.section, row: indexPath.row) {
            let cell = tableView.dequeueReusableCell(withIdentifier: identificator, for: indexPath)
            
            if let updatableCell = cell as? MTJBballGameSettingsCellProtocol {
                updatableCell.update()
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - Time type actions
    
    @IBAction func timeTypeValueChanged(_ sender: Any) {
        if let cell = self.timeTypeCell {
            if let validTimeType = MTJBballGameTimeType(rawValue: cell.timeTypeControl.selectedSegmentIndex) {
                basketballManager.timeType = validTimeType
            }
        }
    }

    // MARK: - Time periods actions
    
    func updatePeriodsButtons() {
        if let cell = self.timePeriodsCell {
            cell.update()
        }
    }
    
    @IBAction func onPeriodsMoreClicked(_ sender: Any) {
        basketballManager.periodsCount += 1
        self.updatePeriodsButtons()
    }
    
    @IBAction func onPeriodsLessClicked(_ sender: Any) {
        basketballManager.periodsCount -= 1
        self.updatePeriodsButtons()
    }
    
    @IBAction func onMinutesMoreClicked(_ sender: Any) {
        basketballManager.minutesCount += 1
        self.updatePeriodsButtons()
    }
    
    @IBAction func onMinutesLessClicked(_ sender: Any) {
        basketballManager.minutesCount -= 1
        self.updatePeriodsButtons()
    }
    
    @IBAction func onAtackSecondsMoreClicked(_ sender: Any) {
        basketballManager.atackSecondsCount += 1
        self.updatePeriodsButtons()
    }
    
    @IBAction func onAtackSecondsLessClicked(_ sender: Any) {
        basketballManager.atackSecondsCount -= 1
        self.updatePeriodsButtons()
    }
    // MARK: - Overtime actions
    
    @IBAction func overtimeTypeValueChanged(_ sender: UISegmentedControl) {
        guard let cell = self.overtimeTypeCell else {
            return
        }
        if (sender.selectedSegmentIndex == cell.overtimeTypeSelectedIndex) {
            sender.selectedSegmentIndex =  UISegmentedControlNoSegment;
            cell.overtimeTypeSelectedIndex = UISegmentedControlNoSegment;
        } else {
            cell.overtimeTypeSelectedIndex = sender.selectedSegmentIndex;
        }
        let wasShown = basketballManager.hasOvertime
        if let overtimeType = self.overtimeTypeMap[cell.overtimeTypeSelectedIndex] {
            basketballManager.overtimeType = overtimeType
        }
        let isShown = basketballManager.hasOvertime
        
        if wasShown != isShown {
            if isShown {
                self.tableView.insertRows(at: [MTJBballGameSettingsSection.overtimeDurationCellIndexPath], with: .automatic)
            } else {
                self.tableView.deleteRows(at: [MTJBballGameSettingsSection.overtimeDurationCellIndexPath], with: .automatic)
            }
        }
        
        self.updateOvertimeButtons()
        
        self.tableView.scrollToRow(at: IndexPath(row: MTJBballGameSettingsSection.rowsCount(forSection: MTJBballGameSettingsSection.overtime.rawValue)! - 1, section: MTJBballGameSettingsSection.overtime.rawValue), at: .bottom, animated: true)
    }
    
    func updateOvertimeButtons() {
        
        if let cell = self.overtimeTypeCell {
            cell.update()
        }
        if let cell = self.overtimeDurationCell {
            cell.update()
        }
    }
    
    @IBAction func onOvertimeMoreClicked(_ sender: Any) {
        basketballManager.overtimeCount += 1
        self.updateOvertimeButtons()
    }
    
    @IBAction func onOvertimeLessClicked(_ sender: Any) {
        basketballManager.overtimeCount -= 1
        self.updateOvertimeButtons()
    }
    
    @IBAction func onNextClicked(_ sender: Any) {
        basketballManager.gameSettingsFinished()
    }
}
