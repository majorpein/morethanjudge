//
//  MTJBballGameSettingsOvertimeCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 27/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballGameSettingsOvertimeCell: UITableViewCell, MTJBballGameSettingsCellProtocol {

    @IBOutlet weak var overtimeTypeControl: MTJDeselectableSegmentedControl!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var overtimeTypeSelectedIndex: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let normalAttributes = [
            NSForegroundColorAttributeName: UIColor(white: 1.0, alpha: 0.19),
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)
        ]
        
        let selectedAttributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)
        ]
        
        overtimeTypeControl.setTitleTextAttributes(normalAttributes   as [NSObject : AnyObject], for: .normal)
        overtimeTypeControl.setTitleTextAttributes(selectedAttributes as [NSObject : AnyObject], for: .selected)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func overtimeTypeIndex() -> Int {
        switch MTJBasketballSettingsManager.basketballManager.overtimeType {
        case .noOvertime:
            return UISegmentedControlNoSegment
        case .madeGoal:
            return 0
        case .fixTime:
            return 1
        }
    }
    
    func update() {
        let basketballManager = MTJBasketballSettingsManager.basketballManager
        
        self.overtimeTypeSelectedIndex = self.overtimeTypeIndex()
        self.overtimeTypeControl.selectedSegmentIndex = self.overtimeTypeSelectedIndex
        
        let description: String = basketballManager.selectedOvertimeDescription()!
        
        self.descriptionLabel.attributedText = self.descriptionLabelAttributedText(for: description)
        
        overtimeTypeControl.setBordersVisible(basketballManager.hasOvertime)
    }
    
    func descriptionLabelAttributedText(for description: String) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = .center
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor(white: 1.0, alpha: 0.5),
            NSFontAttributeName: UIFont.systemFont(ofSize: 14.0),
            NSParagraphStyleAttributeName: paragraphStyle
        ]
        
        return NSAttributedString(string: description, attributes: attributes)
    }
}
