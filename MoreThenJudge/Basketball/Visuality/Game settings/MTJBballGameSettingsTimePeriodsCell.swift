//
//  MTJBballGameSettingsTimePeriodsCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 27/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballGameSettingsTimePeriodsCell: UITableViewCell, MTJBballGameSettingsCellProtocol {

    @IBOutlet weak var periodsCountLabel: UILabel!
    @IBOutlet weak var minutesCountLabel: UILabel!
    @IBOutlet weak var atackSecondsCountLabel: UILabel!
    
    @IBOutlet weak var periodsMoreButton: UIButton!
    @IBOutlet weak var periodsLessButton: UIButton!
    
    @IBOutlet weak var minutesMoreButton: UIButton!
    @IBOutlet weak var minutesLessButton: UIButton!
    
    @IBOutlet weak var atackSecondsMoreButton: UIButton!
    @IBOutlet weak var atackSecondsLessButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update() {
        let basketballManager = MTJBasketballSettingsManager.basketballManager
        
        self.periodsCountLabel.text = String(basketballManager.periodsCount)
        self.periodsLessButton.isEnabled = basketballManager.canDecreasePeriodsCount
        self.periodsMoreButton.isEnabled = basketballManager.canEncreasePeriodsCount
        
        self.minutesCountLabel.text = String(basketballManager.minutesCount)
        self.minutesLessButton.isEnabled = basketballManager.canDecreaseMinutesCount
        self.minutesMoreButton.isEnabled = basketballManager.canEncreaseMinutesCount
        
        self.atackSecondsCountLabel.text = String(basketballManager.atackSecondsCount)
        self.atackSecondsLessButton.isEnabled = basketballManager.canDecreaseAtackSecondsCount
        self.atackSecondsMoreButton.isEnabled = basketballManager.canEncreaseAtackSecondsCount
    }
}
