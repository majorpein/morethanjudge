//
//  MTJBballGameSettingsNextCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 31/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballGameSettingsNextCell: UITableViewCell {

    @IBOutlet weak var nextButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.nextButton.layer.shadowOpacity = 0.24
        self.nextButton.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
