//
//  MTJBballPlayerBubbleView.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 13/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

protocol MTJBballPlayerBubbleViewDelegate {
    func tapOnPlayer(_ number: Int, isHome: Bool)
}

class MTJBballPlayerBubbleView: UIView {
    
    let isHome: Bool
    let number: Int
    var tapRecognizer: UITapGestureRecognizer
    let numberLabel: UILabel
    let nameLabel: UILabel
    let imageView: UIImageView
    
    let gameEngine = MTJBasketballSettingsManager.basketballEngine!
    
    var isHint = true
    var isActive: Bool {
        if let newIs = self.gameEngine.playerIsActive(with: self.number, isHome: self.isHome) {
            return newIs
        }
        return false
    }
    
    override var frame: CGRect {
        didSet {
            self.updateFrames()
        }
    }
    var delegate: MTJBballPlayerBubbleViewDelegate?
    
    init(number: Int, name: String?, isHome: Bool, frame: CGRect = CGRect.zero) {
        
        self.isHome = isHome
        self.number = number
        
        self.imageView = UIImageView()
        self.imageView.layer.shadowOpacity = 0.24
        self.imageView.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        
        self.numberLabel = UILabel()
        self.numberLabel.textColor = UIColor.white
        self.numberLabel.font = UIFont(name: systemMediumFontName, size: 30.0)
        self.numberLabel.text = String(number)
        
        self.nameLabel = UILabel()
        self.nameLabel.textColor = UIColor.white
        self.nameLabel.font = UIFont.systemFont(ofSize: 10.0)
        self.nameLabel.text = name ?? ""
        
        self.tapRecognizer = UITapGestureRecognizer()
        
        super.init(frame: frame)
        
        self.tapRecognizer.addTarget(self, action: #selector(tapRecognized))

        
        self.addGestureRecognizer(self.tapRecognizer)
        self.addSubview(self.imageView)
        self.addSubview(self.numberLabel)
        self.addSubview(self.nameLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFrames() {
        self.imageView.image = UIImage(named: self.isHint ? "white_inactive_ring" : (self.isActive == true ? "player_bubble.png" : "player_bubble_inactive.png"))
        self.imageView.sizeToFit()
        self.imageView.center = CGPoint(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        
        self.numberLabel.sizeToFit()
        self.numberLabel.center = CGPoint(x: self.imageView.center.x, y: self.imageView.center.y - 3.0)
        
        self.nameLabel.sizeToFit()
        self.nameLabel.frame.size.width = min(self.nameLabel.frame.size.width, 38.0)
        self.nameLabel.center = CGPoint(x: self.numberLabel.center.x, y: self.numberLabel.center.y + 18.0)
    }
    
    func tapRecognized() {
        print("tap on \(self.isActive ? "active" : "inactive") \(self.isHome ? "home" : "away") team player with number \(self.number)")
        self.delegate?.tapOnPlayer(self.number, isHome: self.isHome)
    }
}
