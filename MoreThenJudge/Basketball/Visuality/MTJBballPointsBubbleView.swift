//
//  MTJBballPointsBubble.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 15/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballPointsBubbleView: UIView {

    var isHome: Bool
    let score: Int
    let tapRecognizer: UITapGestureRecognizer
    let scoreLabel: UILabel
    let descLabel: UILabel
    let imageView: UIImageView
    
    override var frame: CGRect {
        didSet {
            self.updateFrames()
        }
    }
    var delegate: MTJBballPointsViewDelegate?
    
    init(score: Int, isHome: Bool, frame: CGRect = CGRect.zero) {
        
        self.isHome = isHome
        self.score = score
        
        self.imageView = UIImageView(image: UIImage(named: "points_bubble.png"))
        self.imageView.layer.shadowOpacity = 0.24
        self.imageView.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        
        self.scoreLabel = UILabel()
        self.scoreLabel.textColor = UIColor.white
        self.scoreLabel.font = UIFont(name: systemMediumFontName, size: 30.0)
        self.scoreLabel.text = String(score)
        
        self.descLabel = UILabel()
        self.descLabel.textColor = UIColor.white
        self.descLabel.font = UIFont.systemFont(ofSize: 10.0)
        self.descLabel.text = "BasketballPointsScored".plural(for: score)
        
        self.tapRecognizer = UITapGestureRecognizer()
        
        super.init(frame: frame)
        
        self.tapRecognizer.addTarget(self, action: #selector(tapRecognized))

        self.addGestureRecognizer(self.tapRecognizer)
        self.addSubview(self.imageView)
        self.addSubview(self.scoreLabel)
        self.addSubview(self.descLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFrames() {
        self.imageView.sizeToFit()
        self.imageView.center = CGPoint(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        
        self.scoreLabel.sizeToFit()
        self.scoreLabel.center = CGPoint(x: self.imageView.center.x, y: self.imageView.center.y - 3.0)
        
        self.descLabel.sizeToFit()
        self.descLabel.center = CGPoint(x: self.scoreLabel.center.x, y: self.scoreLabel.center.y + 18.0)
    }
    
    func tapRecognized() {
        self.delegate?.tapOnScore(self.score, isHome: self.isHome)
    }

}
