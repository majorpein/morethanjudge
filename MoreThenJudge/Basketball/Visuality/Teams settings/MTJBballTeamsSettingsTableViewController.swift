//
//  MTJBballTeamsSettingsTableViewController.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 23/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

let orange      = UIColor(red: 255.0/255.0, green: 78.0/255.0, blue:  0.0/255.0, alpha: 250.0/255.0)
let darkOrange  = UIColor(red: 245.0/255.0, green: 52.0/255.0, blue:  0.0/255.0, alpha: 250.0/255.0)
let darkBlue    = UIColor(red:  17.0/255.0, green: 35.0/255.0, blue: 63.0/255.0, alpha: 250.0/255.0)

let playerCellHeight = 48.0

let animationTime = 0.3

protocol MTJBballTeamsSettingsCellProtocol {
    func update(selected isHome: Bool)
}

enum MTJBballTeamsSettingsSection: Int {
    case team = 0, players
    
    static var sections: [MTJBballTeamsSettingsSection : [MTJBballSettingsSectionInfo]] = [
        team:   [MTJBballSettingsSectionInfo(identificator:"MTJBballTeamsSettingsSectionTeamCell", rowHeight:140.0)],
        players:[
            MTJBballSettingsSectionInfo(identificator:"MTJBballTeamsSettingsPlayersTitleCell", rowHeight:60.0),
            MTJBballSettingsSectionInfo(identificator:"MTJBballTeamsSettingsAddPlayerCell", rowHeight:336.0),
            MTJBballSettingsSectionInfo(identificator:"MTJBballTeamsSettingsSectionStartCell", rowHeight:86.0)
        ]
    ]
    
    static let playerTitleCellIndexPath = IndexPath(row: 0, section: players.rawValue)
    
    static func identificator (forSection section: Int, row: Int) -> String? {
        if let section = MTJBballTeamsSettingsSection(rawValue: section) {
            if let sectionArray = sections[section] {
                return sectionArray[row].identificator
            }
        }
        return nil
    }
    
    static func rowHeight (forSection section: Int, row: Int) -> Double? {
        if let section = MTJBballTeamsSettingsSection(rawValue: section) {
            if let sectionArray = sections[section] {
                return sectionArray[row].rowHeight
            }
        }
        return nil
    }
    
    static func rowsCount (forSection section: Int) -> Int? {
        if let section = MTJBballTeamsSettingsSection(rawValue: section) {
            if let sectionArray = sections[section] {
                return sectionArray.count
            }
        }
        return nil
    }
    
    static var count: Int {
        return sections.count
    }
}

class MTJBballTeamsSettingsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, MTJBballTeamsSettingsPlayerCellDelegate {

    @IBOutlet weak var homeTeamButton: UIButton!
    @IBOutlet weak var awayTeamButton: UIButton!
    
    @IBOutlet weak var homeTeamSelectionView: UIView!
    @IBOutlet weak var awayTeamSelectionView: UIView!
    
    @IBOutlet weak var teamsSelectionBackgroundView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    let basketballManager = MTJBasketballSettingsManager.basketballManager
    
    var homeTeamSelected: Bool = true
    
    var teamCell: MTJBballTeamsSettingsTeamCell? {
        if let cell: MTJBballTeamsSettingsTeamCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: MTJBballTeamsSettingsSection.team.rawValue)) as? MTJBballTeamsSettingsTeamCell {
            return cell
        }
        return nil
    }
    
    var addPlayerCell: MTJBballTeamsSettingsAddPlayerCell? {
        let section = MTJBballTeamsSettingsSection.players.rawValue
        if let cell: MTJBballTeamsSettingsAddPlayerCell = self.tableView.cellForRow(at: IndexPath(row: MTJBballTeamsSettingsSection.rowsCount(forSection: section)! + self.playersCount - 2, section: section)) as? MTJBballTeamsSettingsAddPlayerCell {
            return cell
        }
        return nil
    }
    
    var startButtonCell: MTJBballTeamsSettingsStartCell? {
        let section = MTJBballTeamsSettingsSection.players.rawValue
        if let cell: MTJBballTeamsSettingsStartCell = self.tableView.cellForRow(at: IndexPath(row: MTJBballTeamsSettingsSection.rowsCount(forSection: section)! + self.playersCount - 1, section: section)) as? MTJBballTeamsSettingsStartCell {
            return cell
        }
        return nil
    }
    
    var playersCount: Int {
        return self.homeTeamSelected == true ? basketballManager.homeTeamPlayersCount : basketballManager.awayTeamPlayersCount
    }
    
    var playerRowsRange: ClosedRange<Int>? {
        guard self.playersCount > 0 else {
            return nil
        }
        let start = MTJBballTeamsSettingsSection.playerTitleCellIndexPath.row + 1
        let end   = MTJBballTeamsSettingsSection.playerTitleCellIndexPath.row + self.playersCount
        return start...end
    }
    
    var pendingPlayerName: String? {
        if let cell = self.addPlayerCell {
            return cell.textField.text
        }
        return nil
    }
    
    var tap: UITapGestureRecognizer?
    
    func hideKeyboardWhenTappedAround() {
        self.tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap!)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Needed to hide empty rows
        self.tableView.tableFooterView = UIView()
        self.tableView.isEditing = true
        
        //DEBUG
        basketballManager.gameSettingsStarted()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.selectTeam(isHome: self.homeTeamSelected)
        
        self.teamsSelectionBackgroundView.layer.shadowOpacity = 0.24
        self.teamsSelectionBackgroundView.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        
        self.updateButtons()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func keyboardWillShow(notification: NSNotification) {
        if let cell = self.teamCell {
            if cell.textField.isEditing {
                return
            }
        }
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
            UIView.animate(withDuration: animationTime, animations: {
                self.tableViewTopConstraint.constant = -keyboardSize.height
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: animationTime, animations: {
            self.tableViewTopConstraint.constant = 0.0
            self.view.layoutIfNeeded()
        })
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return MTJBballTeamsSettingsSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == MTJBballTeamsSettingsSection.players.rawValue {
            return (MTJBballTeamsSettingsSection.rowsCount(forSection: section) ?? 0) + self.playersCount
        }
        return MTJBballTeamsSettingsSection.rowsCount(forSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == MTJBballTeamsSettingsSection.players.rawValue {
            if let range = self.playerRowsRange {
                if range ~= indexPath.row {
                    return CGFloat(playerCellHeight)
                }
            }
        }
        let row = indexPath.row == 0 ? 0 : indexPath.row - self.playersCount
        if let rowHeight = MTJBballTeamsSettingsSection.rowHeight(forSection: indexPath.section, row: row) {
            return CGFloat(rowHeight)
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == MTJBballTeamsSettingsSection.players.rawValue {
            if let range = self.playerRowsRange {
                if range ~= indexPath.row {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MTJBballTeamsSettingsPlayerCell", for: indexPath) as! MTJBballTeamsSettingsPlayerCell
                    
                    cell.update(isHome: self.homeTeamSelected, player: indexPath.row - 1)
                    cell.delegate = self
                    
                    return cell
                }
            }
        }
        let row = indexPath.row == 0 ? 0 : indexPath.row - self.playersCount
        if let identificator = MTJBballTeamsSettingsSection.identificator(forSection: indexPath.section, row: row) {
            let cell = tableView.dequeueReusableCell(withIdentifier: identificator, for: indexPath)
            
            if let updatableCell = cell as? MTJBballTeamsSettingsCellProtocol {
                updatableCell.update(selected: self.homeTeamSelected)
            }
            
            return cell
        }
        return UITableViewCell()
    }

    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if let range = self.playerRowsRange {
            if range ~= indexPath.row {
                return true
            }
        }
        return false
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let index = indexPath.row - 1
            self.homeTeamSelected == true ? basketballManager.removePlayerFromHomeTeam(at: index) : basketballManager.removePlayerFromAwayTeam(at: index)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.updateStartButtonCell()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    // Override to support rearranging the table view.
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        /*
        if let range = self.playerRowsRange {
            if range ~= indexPath.row {
                return true
            }
        }
        */
        return false
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
 
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if (self.homeTeamSelected) {
            basketballManager.moveHomeTeamPlayer(from: sourceIndexPath.row - 1, to: destinationIndexPath.row - 1)
        } else {
            basketballManager.moveAwayTeamPlayer(from: sourceIndexPath.row - 1, to: destinationIndexPath.row - 1)
        }
        tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Teams Settings
    
    @IBAction func onHomeTeamClicked(_ sender: Any) {
        if let cell = self.teamCell {
            cell.textField.endEditing(true)
        }
        self.selectTeam(isHome: true)
    }
    
    @IBAction func onAwayTeamClicked(_ sender: Any) {
        if let cell = self.teamCell {
            cell.textField.endEditing(true)
        }
        self.selectTeam(isHome: false)
    }
    
    func selectTeam(isHome: Bool) {
        self.homeTeamSelected = isHome
        self.homeTeamSelectionView.isHidden = !isHome
        self.awayTeamSelectionView.isHidden = isHome
        
        self.homeTeamButton.isSelected = isHome
        self.awayTeamButton.isSelected = !isHome
        
        self.homeTeamButton.isUserInteractionEnabled = !isHome
        self.awayTeamButton.isUserInteractionEnabled = isHome
        /*
        if let cell = self.teamCell {
            cell.update(selected: self.homeTeamSelected)
        }
        */
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    func updateButtons() {
        self.homeTeamButton.setAttributedTitle(self.teamButtonNormalAttributedText  (for: basketballManager.homeTeamName.isEmpty ? NSLocalizedString("DefaultHomeTeam", comment: "") : basketballManager.homeTeamName), for: .normal)
        self.homeTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: basketballManager.homeTeamName.isEmpty ? NSLocalizedString("DefaultHomeTeam", comment: "") : basketballManager.homeTeamName), for: .highlighted)
        self.homeTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: basketballManager.homeTeamName.isEmpty ? NSLocalizedString("DefaultHomeTeam", comment: "") : basketballManager.homeTeamName), for: .selected)
        
        self.awayTeamButton.setAttributedTitle(self.teamButtonNormalAttributedText  (for: basketballManager.awayTeamName.isEmpty ? NSLocalizedString("DefaultAwayTeam", comment: "") : basketballManager.awayTeamName), for: .normal)
        self.awayTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: basketballManager.awayTeamName.isEmpty ? NSLocalizedString("DefaultAwayTeam", comment: "") : basketballManager.awayTeamName), for: .highlighted)
        self.awayTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: basketballManager.awayTeamName.isEmpty ? NSLocalizedString("DefaultAwayTeam", comment: "") : basketballManager.awayTeamName), for: .selected)
        
        self.updateStartButtonCell()
    }
    
    func updateStartButtonCell() {
        if let cell = self.startButtonCell {
            cell.update(selected: self.homeTeamSelected)
        }
    }
    
    func teamButtonNormalAttributedText(for description: String) -> NSAttributedString {
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)!
        ]
        
        return NSAttributedString(string: description, attributes: attributes)
    }
    
    func teamButtonSelectedAttributedText(for description: String) -> NSAttributedString {
        
        let attributes = [
            NSForegroundColorAttributeName: orange,
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)!
        ]
        
        return NSAttributedString(string: description, attributes: attributes)
    }
    
    // MARK: - Text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.hideKeyboardWhenTappedAround()
        textField.placeholder = ""
        if let cell = self.addPlayerCell {
            if textField.isEqual(cell.numberLabel) {
                cell.numberLessButton.isEnabled = false
                cell.numberMoreButton.isEnabled = false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let tap = self.tap {
            self.view.removeGestureRecognizer(tap)
        }
        if let cell = self.teamCell {
            if textField.isEqual(cell.textField) {
                if let name = cell.textField.text {
                    if self.homeTeamSelected {
                        basketballManager.homeTeamName = name
                    } else {
                        basketballManager.awayTeamName = name
                    }
                    self.updateButtons()
                    cell.update(selected: self.homeTeamSelected)
                }
                cell.showTextFieldPlaceholder()
            }
        }
        if let cell = self.addPlayerCell {
            if textField.isEqual(cell.textField) {
                cell.showTextFieldPlaceholder()
            }
            if textField.isEqual(cell.numberLabel) {
                if let text = textField.text {
                    if let number = Int(text) {
                        basketballManager.pendingPlayerNumber = number
                        self.updateAddPlayerCell()
                    }
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let cell = self.addPlayerCell {
            if textField.isEqual(cell.numberLabel) {
                let newString = textField.text! == "0" ? string : (textField.text! as NSString).replacingCharacters(in: range, with: string)
                if let number = Int(newString) {
                    if self.homeTeamSelected == true {
                        if basketballManager.canSetPendingNumber(number: number) {
                            textField.text! = newString
                            return false
                        }
                    } else {
                        if basketballManager.canSetPendingNumber(number: number) {
                            textField.text! = newString
                            return false
                        }
                    }
                }
                if newString.isEmpty {
                    textField.text = "0"
                }
                return false
            }
        }
        return true
    }
    
    @IBAction func teamNameDidEndOnExit(_ sender: Any) {
        if let cell = self.teamCell {
            cell.textField.endEditing(true)
        }
    }
    
    @IBAction func playerNameDidEndOnExit(_ sender: Any) {
        if let cell = self.addPlayerCell {
            cell.textField.endEditing(true)
        }
    }

    // MARK: - Players Settings
    
    @IBAction func onAddPlayerClicked(_ sender: Any) {
        guard let cell = self.addPlayerCell else {
            return;
        }
        cell.numberLabel.endEditing(true)
        cell.textField.endEditing(true)
        
        if self.homeTeamSelected == true {
            basketballManager.addPlayerToHomeTeam(self.pendingPlayerName)
        } else {
            basketballManager.addPlayerToAwayTeam(self.pendingPlayerName)
        }
        let row = MTJBballTeamsSettingsSection.playerTitleCellIndexPath.row + self.playersCount
        self.tableView.insertRows(at: [IndexPath(row: row, section: MTJBballTeamsSettingsSection.players.rawValue)], with: .automatic)
        self.updateStartButtonCell()
        self.updateAddPlayerCell()
        
        let currentRect = CGRect.init(origin: self.tableView.contentOffset, size: self.tableView.contentSize)
        self.tableView.scrollRectToVisible(currentRect.offsetBy(dx: 0.0, dy: CGFloat(playerCellHeight)), animated: true)
        
        self.onNumberMoreClicked(self)
        
        cell.textField.text! = ""
    }
    
    @IBAction func onNextClicked(_ sender: Any) {
        MTJBasketballSettingsManager.initBasketballEngine()
        if let cell = self.teamCell {
            cell.textField.endEditing(true)
        }
    }
    
    func updateAddPlayerCell() {
        if let cell = self.addPlayerCell {
            cell.update(selected: self.homeTeamSelected)
        }
    }
    
    @IBAction func onNumberMoreClicked(_ sender: Any) {
        basketballManager.pendingPlayerNumber += 1
        self.updateAddPlayerCell()
    }
    
    @IBAction func onNumberLessClicked(_ sender: Any) {
        basketballManager.pendingPlayerNumber -= 1
        self.updateAddPlayerCell()
    }
    
    func checkButtonClicked(_ sender: MTJBballTeamsSettingsPlayerCell) {
        if let indexPath = self.tableView.indexPath(for: sender) {
            let index = indexPath.row - 1
            if self.homeTeamSelected == true {
                basketballManager.selectHomeTeamPlayer(at: index, isSelected: !basketballManager.homeTeamPlayerIsSelected(at: index))
                //sender.update(status: MTJBasketballManager.homeTeamPlayerStatus(at: index), isSelected: MTJBasketballManager.homeTeamPlayerIsSelected(at: index))
            } else {
                basketballManager.selectAwayTeamPlayer(at: index, isSelected: !basketballManager.awayTeamPlayerIsSelected(at: index))
                //sender.update(status: MTJBasketballManager.awayTeamPlayerStatus(at: index), isSelected: MTJBasketballManager.awayTeamPlayerIsSelected(at: index))
            }
            self.tableView.reloadData()
        }
    }
    
    @IBAction func returnFromGame(segue: UIStoryboardSegue) {
        
    }
}
