//
//  MTJBballTeamsSettingsStartCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 26/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballTeamsSettingsStartCell: UITableViewCell, MTJBballTeamsSettingsCellProtocol {

    @IBOutlet weak var startButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.startButton.layer.shadowOpacity = 0.24
        self.startButton.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(selected isHome: Bool) {
        self.startButton.isEnabled = MTJBasketballSettingsManager.basketballManager.isReady
    }
}
