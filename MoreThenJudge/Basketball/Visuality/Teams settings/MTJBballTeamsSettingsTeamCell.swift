//
//  MTJBballTeamsSettingsTeamCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 25/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballTeamsSettingsTeamCell: UITableViewCell, MTJBballTeamsSettingsCellProtocol {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textFieldDoneIndicator: UIImageView!
    
    func showTextFieldPlaceholder() {
        let placeholderAttributes = [
            NSForegroundColorAttributeName: UIColor.white,//(white: 1.0, alpha: 0.5),
            NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)
        ]
        
        let attributedPlaceholder = NSAttributedString(string:NSLocalizedString("TeamNamePlaceholder", comment: ""), attributes: placeholderAttributes)
        textField.attributedPlaceholder = attributedPlaceholder
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.showTextFieldPlaceholder()
        
        textField.layer.borderColor = UIColor(red: 41.0/255.0, green: 57.0/255.0, blue: 82.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(selected isHome: Bool) {
        let basketballManager = MTJBasketballSettingsManager.basketballManager
        self.textField.text = isHome ? basketballManager.homeTeamName : basketballManager.awayTeamName
        self.textFieldDoneIndicator.isHidden = self.textField.text!.isEmpty
    }
}
