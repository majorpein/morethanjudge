//
//  MTJBballTeamsSettingsAddPlayerCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 03/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballTeamsSettingsAddPlayerCell: UITableViewCell, MTJBballTeamsSettingsCellProtocol {

    @IBOutlet weak var descriptionLabel: UILabel!
    //@IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberLabel: UITextField!
    
    @IBOutlet weak var numberMoreButton: UIButton!
    @IBOutlet weak var numberLessButton: UIButton!
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var addPlayerButton: UIButton!
    
    func showTextFieldPlaceholder() {
        let placeholderAttributes = [
            NSForegroundColorAttributeName: UIColor.white,//(white: 1.0, alpha: 0.5),
            NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)
        ]
        
        let attributedPlaceholder = NSAttributedString(string:NSLocalizedString("PlayerNamePlaceholder", comment: ""), attributes: placeholderAttributes)
        textField.attributedPlaceholder = attributedPlaceholder
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.showTextFieldPlaceholder()
        
        textField.layer.borderColor = UIColor(red: 41.0/255.0, green: 57.0/255.0, blue: 82.0/255.0, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.0
        
        self.addPlayerButton.layer.shadowOpacity = 0.24
        self.addPlayerButton.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(selected isHome: Bool) {
        let basketballManager = MTJBasketballSettingsManager.basketballManager
        
        self.numberLabel.text = String(basketballManager.pendingPlayerNumber)
        
        self.numberLessButton.isEnabled = basketballManager.canDecreasePendingNumber
        self.numberMoreButton.isEnabled = basketballManager.canEncreasePendingNumber
        
        self.addPlayerButton.isEnabled = isHome ? basketballManager.canAddHomeTeamPlayer() : basketballManager.canAddAwayTeamPlayer()
        
        let description: String = isHome ? basketballManager.homeTeamPlayersDescription() : basketballManager.awayTeamPlayersDescription()
        
        self.descriptionLabel.attributedText = self.descriptionLabelAttributedText(for: description)
    }
    
    func descriptionLabelAttributedText(for description: String) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = .center
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor(white: 1.0, alpha: 0.5),
            NSFontAttributeName: UIFont.systemFont(ofSize: 14.0),
            NSParagraphStyleAttributeName: paragraphStyle
        ]
        
        return NSAttributedString(string: description, attributes: attributes)
    }
}
