//
//  MTJBballTeamsSettingsPlayerCell.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 03/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

protocol MTJBballTeamsSettingsPlayerCellDelegate {
    func checkButtonClicked(_ sender: MTJBballTeamsSettingsPlayerCell)
}

class MTJBballTeamsSettingsPlayerCell: UITableViewCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    var delegate: MTJBballTeamsSettingsPlayerCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(isHome: Bool, player index: Int) {
        let basketballManager = MTJBasketballSettingsManager.basketballManager
        
        let isSelected = isHome ? basketballManager.homeTeamPlayerIsSelected(at: index) : basketballManager.awayTeamPlayerIsSelected(at: index)
        let isEnabled = isHome ? basketballManager.homeTeamPlayerCanBeSelected() : basketballManager.awayTeamPlayerCanBeSelected()
        
        self.numberLabel.text = isHome ? String(basketballManager.homeTeamPlayersNumber(at: index) ?? 0) : String(basketballManager.awayTeamPlayersNumber(at: index) ?? 0)
        self.nameLabel.text = basketballManager.playersName(at: index, isHome: isHome)
        if self.nameLabel.text == nil || self.nameLabel.text!.isEmpty {
            self.nameLabel.text = self.numberLabel.text
        }
        
        self.checkButton.isSelected = isSelected
        self.checkButton.isEnabled = isSelected ? true : isEnabled
        self.backgroundColor = isSelected ? orange : darkBlue
    }

    @IBAction func onCheckButtonClicked(_ sender: Any) {
        self.delegate?.checkButtonClicked(self)
    }
}
