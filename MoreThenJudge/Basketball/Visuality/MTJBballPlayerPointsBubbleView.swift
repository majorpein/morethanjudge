//
//  MTJBballPlayerPointsBubbleView.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 15/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

class MTJBballPlayerPointsBubbleView: UIView {

    let numberLabel: UILabel
    let imageView: UIImageView
    var number: Int? {
        didSet {
            self.numberLabel.text = String(number!)
        }
    }
    override var frame: CGRect {
        didSet {
            self.updateFrames()
        }
    }
    
    override init(frame: CGRect = CGRect.zero) {
        
        self.imageView = UIImageView(image: UIImage(named: "player_points_bubble.png"))
        self.imageView.layer.shadowOpacity = 0.24
        self.imageView.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        
        self.numberLabel = UILabel()
        self.numberLabel.textColor = UIColor.white
        self.numberLabel.font = UIFont(name: systemMediumFontName, size: 20.0)
        self.numberLabel.text = ""
        
        super.init(frame: frame)
        
        self.addSubview(self.imageView)
        self.addSubview(self.numberLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFrames() {
        self.imageView.sizeToFit()
        self.imageView.center = CGPoint(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        
        self.numberLabel.sizeToFit()
        self.numberLabel.center = CGPoint(x: self.imageView.center.x, y: self.imageView.center.y)
    }
}
