//
//  MTJBballPointsView.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 15/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

let spaceBetweenBubbles: CGFloat = 10.0

protocol MTJBballPointsViewDelegate {
    func tapOnScore(_ score: Int, isHome: Bool)
    //func tapOnClose()
}

class MTJBballPointsView: UIView {

    let threePointsBubble: MTJBballPointsBubbleView = MTJBballPointsBubbleView(score: 3, isHome: true)
    let   twoPointsBubble: MTJBballPointsBubbleView = MTJBballPointsBubbleView(score: 2, isHome: true)
    let    onePointBubble: MTJBballPointsBubbleView = MTJBballPointsBubbleView(score: 1, isHome: true)
    
    let playerBubble: MTJBballPlayerPointsBubbleView = MTJBballPlayerPointsBubbleView()
    
    let closeView: UIImageView = UIImageView(image: UIImage(named: "points_close.png"))
    
    //let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()
    
    override var frame: CGRect {
        didSet {
            self.updateFrames()
        }
    }
    var delegate: MTJBballPointsViewDelegate? {
        didSet {
            self.threePointsBubble.delegate = delegate
            self.twoPointsBubble.delegate   = delegate
            self.onePointBubble.delegate    = delegate
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func prepare(number: Int, isHome: Bool, delegate: MTJBballPointsViewDelegate?) {
        
        //self.tapRecognizer.addTarget(self, action: #selector(tapRecognized))
        
        self.playerBubble.number = number
        
        self.threePointsBubble.isHome = isHome
        self.twoPointsBubble.isHome   = isHome
        self.onePointBubble.isHome    = isHome
        
        self.delegate = delegate
        
        self.addSubview(self.threePointsBubble)
        self.addSubview(self.twoPointsBubble)
        self.addSubview(self.onePointBubble)
        self.addSubview(self.playerBubble)
        self.addSubview(self.closeView)
        //self.closeView.isUserInteractionEnabled = true
        //self.closeView.addGestureRecognizer(self.tapRecognizer)
    }
    
    func place(at point: CGPoint) {
        let top: CGFloat = self.playerBubble.frame.size.height/2.0 + 3.0 * (spaceBetweenBubbles + self.onePointBubble.frame.size.height)
        //let bottom: CGFloat = self.playerBubble.frame.size.height/2.0 + spaceBetweenBubbles + self.closeView.frame.size.height
        let width: CGFloat = self.onePointBubble.frame.size.width
        let height: CGFloat = self.onePointBubble.frame.size.height * 3.0 + spaceBetweenBubbles * 2.0
        
        let x: CGFloat = point.x - width/2.0
        let y: CGFloat = point.y - top
        
        self.frame = CGRect(x: x, y: y, width: width, height: height)
    }
    
    func updateFrames() {
        self.threePointsBubble.frame.size = self.threePointsBubble.imageView.image!.size
        var y: CGFloat = 0.0
        self.threePointsBubble.frame.origin = CGPoint(x: 0.0, y: y)
        
        self.twoPointsBubble.frame.size = self.twoPointsBubble.imageView.image!.size
        y += self.threePointsBubble.frame.size.height + spaceBetweenBubbles
        self.twoPointsBubble.frame.origin = CGPoint(x: 0.0, y: y)
        
        self.onePointBubble.frame.size = self.onePointBubble.imageView.image!.size
        y += self.twoPointsBubble.frame.size.height + spaceBetweenBubbles
        self.onePointBubble.frame.origin = CGPoint(x: 0.0, y: y)
        
        self.playerBubble.frame.size = self.playerBubble.imageView.image!.size
        y += self.onePointBubble.frame.size.height + spaceBetweenBubbles
        self.playerBubble.frame.origin = CGPoint(x: (self.frame.size.width - self.playerBubble.frame.size.width)/2.0, y: y)
        
        self.closeView.sizeToFit()
        y += self.playerBubble.frame.size.height + spaceBetweenBubbles
        self.closeView.frame.origin = CGPoint(x: (self.frame.size.width - self.closeView.frame.size.width)/2.0, y: y)
    }
    /*
    func tapRecognized() {
        self.delegate?.tapOnClose()
    }
    */
}
