//
//  MTJBballStatsViewController.swift
//  MoreThenJudge
//
//  Created by Alexandro on 26/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

let playerRowHeight = 54.0

class MTJBballStatsViewController: UIViewController, UIToolbarDelegate {

    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    
    @IBOutlet weak var homeTeamScore: UILabel!
    @IBOutlet weak var awayTeamScore: UILabel!
    
    @IBOutlet weak var homeTeamSelectionView: UIView!
    @IBOutlet weak var homeTeamButton: UIButton!
    
    @IBOutlet weak var awayTeamSelectionView: UIView!
    @IBOutlet weak var awayTeamButton: UIButton!
    
    @IBOutlet weak var statsHomeScrollView: UIScrollView!
    @IBOutlet weak var statsAwayScrollView: UIScrollView!
    
    var homeTeamSelected: Bool = true
    
    let gameEngine = MTJBasketballSettingsManager.basketballEngine!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initScrollViewRows(isHome: true)
        self.initScrollViewRows(isHome: false)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.homeTeamLabel.text = self.gameEngine.homeTeamName
        self.awayTeamLabel.text = self.gameEngine.awayTeamName
        
        self.homeTeamScore.text = "\(gameEngine.currentHomeTeamScore)"
        self.awayTeamScore.text = "\(gameEngine.currentAwayTeamScore)"
        
        self.updateScrollViewFrames(isHome: true, animated: false)
        self.updateScrollViewFrames(isHome: false, animated: false)
        
        self.updateButtons()
        
        self.selectTeam(isHome: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }

    func initScrollViewRows(isHome: Bool) {
        let count = isHome == true ? self.gameEngine.homeTeamPlayersCount : self.gameEngine.awayTeamPlayersCount
        let scrollView: UIScrollView = isHome == true ? self.statsHomeScrollView : self.statsAwayScrollView
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        for index in 0..<count {
            let number   = self.gameEngine.playerNumber(at: index, isHome: isHome)
            let name     = self.gameEngine.playerName(with: number, isHome: isHome)
            
            let row = MTJBballStatsRow.init(number: number, name: name, score: self.gameEngine.playersScore(with: number, isHome: isHome))
            
            scrollView.addSubview(row)
        }
        scrollView.contentSize = CGSize(width: Double(scrollView.frame.size.width), height: playerRowHeight * Double(count))
    }
    
    func updateScrollViewFrames(isHome: Bool, animated: Bool) {
        let scrollView: UIScrollView = isHome == true ? self.statsHomeScrollView : self.statsAwayScrollView
        
        for case let rowView as MTJBballStatsRow in scrollView.subviews {
            
            guard let index = self.gameEngine.indexOfPlayer(with: rowView.number, isHome: isHome) else {
                continue
            }
            
            let frame = CGRect(x: 0.0, y: playerRowHeight * Double(index), width: Double(scrollView.frame.size.width), height: playerRowHeight)
            
            if animated == true {
                UIView.animate(withDuration: animationTime, animations: {
                    rowView.frame = frame
                    //bubbleView.updateFrames()
                })
            } else {
                rowView.frame = frame
                //bubbleView.updateFrames()
            }
        }
    }
    
    @IBAction func onHomeTeamClicked(_ sender: Any) {
        self.selectTeam(isHome: true)
    }
    
    @IBAction func onAwayTeamClicked(_ sender: Any) {
        self.selectTeam(isHome: false)
    }
    
    func selectTeam(isHome: Bool) {
        self.homeTeamSelected = isHome
        self.homeTeamSelectionView.isHidden = !isHome
        self.awayTeamSelectionView.isHidden = isHome
        
        self.homeTeamButton.isSelected = isHome
        self.awayTeamButton.isSelected = !isHome
        
        self.homeTeamButton.isUserInteractionEnabled = !isHome
        self.awayTeamButton.isUserInteractionEnabled = isHome
        
        self.homeTeamScore.textColor = isHome ? orange : UIColor.white
        self.homeTeamLabel.textColor = isHome ? orange : UIColor.white
        
        self.awayTeamScore.textColor = isHome ? UIColor.white : orange
        self.awayTeamLabel.textColor = isHome ? UIColor.white : orange
        
        self.statsHomeScrollView.isHidden = !isHome
        self.statsAwayScrollView.isHidden = isHome
        //self.tableView.reloadData()
        //self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    func updateButtons() {
        self.homeTeamButton.setAttributedTitle(self.teamButtonNormalAttributedText  (for: gameEngine.homeTeamName.isEmpty ? NSLocalizedString("DefaultHomeTeam", comment: "") : gameEngine.homeTeamName), for: .normal)
        self.homeTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: gameEngine.homeTeamName.isEmpty ? NSLocalizedString("DefaultHomeTeam", comment: "") : gameEngine.homeTeamName), for: .highlighted)
        self.homeTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: gameEngine.homeTeamName.isEmpty ? NSLocalizedString("DefaultHomeTeam", comment: "") : gameEngine.homeTeamName), for: .selected)
        
        self.awayTeamButton.setAttributedTitle(self.teamButtonNormalAttributedText  (for: gameEngine.awayTeamName.isEmpty ? NSLocalizedString("DefaultAwayTeam", comment: "") : gameEngine.awayTeamName), for: .normal)
        self.awayTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: gameEngine.awayTeamName.isEmpty ? NSLocalizedString("DefaultAwayTeam", comment: "") : gameEngine.awayTeamName), for: .highlighted)
        self.awayTeamButton.setAttributedTitle(self.teamButtonSelectedAttributedText(for: gameEngine.awayTeamName.isEmpty ? NSLocalizedString("DefaultAwayTeam", comment: "") : gameEngine.awayTeamName), for: .selected)
    }
    
    func teamButtonNormalAttributedText(for description: String) -> NSAttributedString {
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)!
        ]
        
        return NSAttributedString(string: description, attributes: attributes)
    }
    
    func teamButtonSelectedAttributedText(for description: String) -> NSAttributedString {
        
        let attributes = [
            NSForegroundColorAttributeName: orange,
            NSFontAttributeName: UIFont(name: systemMediumFontName, size: 14.0)!
        ]
        
        return NSAttributedString(string: description, attributes: attributes)
    }
}
