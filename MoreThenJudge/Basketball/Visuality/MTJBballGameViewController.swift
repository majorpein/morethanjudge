//
//  MTJBballGameViewController.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 23/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

let notificationNameGameInterfaceUpdate          = "notificationNameGameInterfaceUpdate"
let notificationNameGameStartAtackClockAnimation = "notificationNameGameStartAtackClockAnimation"
let notificationNameGameStopAtackClockAnimation  = "notificationNameGameStopAtackClockAnimation"
let notificationNameGamePauseAtackClockAnimation = "notificationNameGamePauseAtackClockAnimation"
let notificationNameGameEnded                    = "notificationNameGameEnded"

let playerBubbleRadius = 58.0
let playerBubbleSpace = 16.0

enum MTJBballGameViewState {
    case hints, normal, pointsSelection, subsSelection
}

class MTJBballGameViewController: UIViewController, MTJBballPlayerBubbleViewDelegate, MTJBballPointsViewDelegate {

    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayTeamLabel: UILabel!
    
    @IBOutlet weak var whistleButton: UIButton!
    @IBOutlet weak var whistleHintView: UIView!
    
    @IBOutlet weak var hintsView: UIView!
    
    @IBOutlet weak var statsButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var atackSecondsLabel: UILabel!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var periodsLabel: UILabel!
    
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var homeTeamScoreLabel: UILabel!
    @IBOutlet weak var awayTeamScoreLabel: UILabel!
    
    @IBOutlet weak var homeTeamFullLabel: UILabel!
    @IBOutlet weak var awayTeamFullLabel: UILabel!
    
    @IBOutlet weak var homeTeamPlayersScrollView: UIScrollView!
    @IBOutlet weak var awayTeamPlayersScrollView: UIScrollView!
    
    @IBOutlet weak var atackClockCircle: UIImageView!
    
    @IBOutlet weak var darkView: UIView!
    @IBOutlet weak var pointsView: MTJBballPointsView!
    @IBOutlet weak var subsScrollView: UIScrollView!
    
    @IBOutlet var darkViewTapRecognizer: UITapGestureRecognizer!
    @IBOutlet var atackClockTapRecognizer: UITapGestureRecognizer!
    
    let progressCircle = CAShapeLayer ()
    let gameEngine = MTJBasketballSettingsManager.basketballEngine!
    var state: MTJBballGameViewState = .normal
    
    var selectedPlayerNumber: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initScrollViewBubbles(isHome: true)
        self.initScrollViewBubbles(isHome: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.whistleButton.layer.shadowOpacity = 0.24
        self.whistleButton.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        
        self.homeTeamLabel.text = self.gameEngine.homeTeamName
        self.awayTeamLabel.text = self.gameEngine.awayTeamName
        
        self.homeTeamFullLabel.text = self.gameEngine.homeTeamName
        self.awayTeamFullLabel.text = self.gameEngine.awayTeamName
        
        self.atackClockCircle.layer.addSublayer(progressCircle)
        
        self.updateScrollViewFrames(isHome: true, animated: false)
        self.updateScrollViewFrames(isHome: false, animated: false)

        NotificationCenter.default.addObserver(self, selector: #selector(interfaceUpdate), name: Notification.Name(notificationNameGameInterfaceUpdate), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(startAtackClockAnimation), name: Notification.Name(notificationNameGameStartAtackClockAnimation), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopAtackClockAnimation), name: Notification.Name(notificationNameGameStopAtackClockAnimation), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseAtackClockAnimation), name: Notification.Name(notificationNameGamePauseAtackClockAnimation), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gameEnded), name: Notification.Name(notificationNameGameEnded), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initScrollViewBubbles(isHome: Bool) {
        let count = isHome == true ? self.gameEngine.homeTeamPlayersCount : self.gameEngine.awayTeamPlayersCount
        let scrollView: UIScrollView = isHome == true ? self.homeTeamPlayersScrollView : self.awayTeamPlayersScrollView
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        for index in 0..<count {
            let number   = self.gameEngine.playerNumber(at: index, isHome: isHome)
            let name     = self.gameEngine.playerName(with: number, isHome: isHome)
            
            let bubble = MTJBballPlayerBubbleView(number: number, name: name, isHome: isHome)
            bubble.delegate = self
            
            scrollView.addSubview(bubble)
        }
    }
    
    func updateScrollViewFrames(isHome: Bool, animated: Bool) {
        let scrollView: UIScrollView = isHome == true ? self.homeTeamPlayersScrollView : self.awayTeamPlayersScrollView
        
        let startX = Double(self.view.frame.size.width/2.0) - playerBubbleRadius * 1.5 - playerBubbleSpace
        let incrementX = playerBubbleRadius + playerBubbleSpace
        var x = startX
        for case let bubbleView as MTJBballPlayerBubbleView in scrollView.subviews {
            
            guard let index = self.gameEngine.indexOfPlayer(with: bubbleView.number, isHome: isHome) else {
                continue
            }
            
            let frame = CGRect(x: startX + Double(index) * incrementX, y: 0.0, width: playerBubbleRadius, height: playerBubbleRadius)
            
            if animated == true {
                UIView.animate(withDuration: animationTime, animations: {
                    bubbleView.frame = frame
                    //bubbleView.updateFrames()
                })
            } else {
                bubbleView.frame = frame
                //bubbleView.updateFrames()
            }
            x += incrementX
        }
        scrollView.contentSize = CGSize(width: x, height: playerBubbleRadius)
    }
    
    func initSubsScrollViewBubbles(isHome: Bool) {
        self.subsScrollView.subviews.forEach({ $0.removeFromSuperview() })
        let count = self.gameEngine.activePlayersCount
        for index in 0..<count {
            if let number   = self.gameEngine.activePlayerNumber(at: index, isHome: isHome) {
                let name    = self.gameEngine.playerName(with: number, isHome: isHome)
                
                let bubble = MTJBballPlayerBubbleView(number: number, name: name, isHome: isHome)
                bubble.delegate = self
                bubble.isHint = false
                
                self.subsScrollView.addSubview(bubble)
            }
        }
    }
    
    func updateSubsScrollViewFrames(isHome: Bool, animated: Bool) {
        let scrollView: UIScrollView = isHome == true ? self.homeTeamPlayersScrollView : self.awayTeamPlayersScrollView
        
        let startX = Double(self.view.frame.size.width/2.0) - playerBubbleRadius * 1.5 - playerBubbleSpace
        let incrementX = playerBubbleRadius + playerBubbleSpace
        var x = startX
        for case let bubbleView as MTJBballPlayerBubbleView in self.subsScrollView.subviews {
            
            guard let index = self.gameEngine.indexOfActivePlayer(with: bubbleView.number, isHome: isHome) else {
                continue
            }
            
            let frame = CGRect(x: startX + Double(index) * incrementX, y: 0.0, width: playerBubbleRadius, height: playerBubbleRadius)
            
            if animated == true {
                UIView.animate(withDuration: animationTime, animations: {
                    bubbleView.frame = frame
                    //bubbleView.updateFrames()
                })
            } else {
                bubbleView.frame = frame
                //bubbleView.updateFrames()
            }
            x += incrementX
        }
        self.subsScrollView.contentSize = CGSize(width: x, height: playerBubbleRadius)
        self.subsScrollView.frame = scrollView.frame
    }
    
    @IBAction func onWhistleClicked(_ sender: Any) {
        self.gameEngine.pauseGame()
    }

    @IBAction func onStartGameClicked(_ sender: Any) {
        self.hideHints()
        UIView.animate(withDuration: animationTime, animations: {
            self.hintsView.alpha = 0.0
        }, completion: { (completion: Bool) in
            self.hintsView.isHidden = true
        })
        self.gameEngine.startGame()
    }
    
    @IBAction func onCloseClicked(_ sender: Any) {
        self.hintsView.isHidden = false
        UIView.animate(withDuration: animationTime, animations: {
            self.hintsView.alpha = 0.9
        })
        self.gameEngine.stopGame()
    }
    
    @IBAction func onPauseClicked(_ sender: Any) {
        self.gameEngine.pauseGame()
    }
    
    @IBAction func onPlayClicked(_ sender: Any) {
        self.gameEngine.resumeGame()
    }
    
    func hideHints() {
        for case let bubbleView as MTJBballPlayerBubbleView in self.homeTeamPlayersScrollView.subviews {
            bubbleView.isHint = false
            bubbleView.updateFrames()
        }
        for case let bubbleView as MTJBballPlayerBubbleView in self.awayTeamPlayersScrollView.subviews {
            bubbleView.isHint = false
            bubbleView.updateFrames()
        }
        self.whistleButton.isHidden = false
        self.whistleHintView.isHidden = true
    }
    
    func interfaceUpdate() {
        self.atackSecondsLabel.text = self.atackClockString()
        self.timerLabel.text = self.timerString()
        self.periodsLabel.text = self.gameEngine.currentPeriodString
        
        self.homeTeamScoreLabel.text = "\(gameEngine.currentHomeTeamScore)"
        self.awayTeamScoreLabel.text = "\(gameEngine.currentAwayTeamScore)"
        
        self.playButton.isHidden = !(gameEngine.isOnPause)
        self.pauseButton.isHidden = !(gameEngine.isInPlay)
    }
    
    func atackClockString() -> String {
        return "\(self.gameEngine.currentAtackSecondsLeft)"
    }
    
    func timerString() -> String {
        let fullTime = self.gameEngine.currentPeriodSecondsLeft
        
        let seconds = fullTime % 60
        let fullMinutes = fullTime / 60
        let minutes = fullMinutes % 60
        let hours = fullMinutes / 60
        
        let secondsString = seconds < 10 ? "0\(seconds)" : "\(seconds)"
        let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let hoursString   = hours   < 10 ? "0\(hours)"   : "\(hours)"
        
        return "\(hoursString):\(minutesString):\(secondsString)"
    }
    
    func gameEnded() {
        
    }
    
    func startAtackClockAnimation() {
        self.animateAtackClock(from: self.gameEngine.atackClockAnimationFromValue, to: self.gameEngine.atackClockAnimationToValue, animated: true)
    }
    
    func stopAtackClockAnimation() {
        self.progressCircle.removeAllAnimations()
    }
    
    func pauseAtackClockAnimation() {
        self.progressCircle.removeAllAnimations()
        
        self.animateAtackClock(from: self.gameEngine.atackClockAnimationFromValue, to: self.gameEngine.atackClockAnimationFromValue, animated: false)
    }
    
    func animateAtackClock(from startValue: Double, to endValue: Double, animated: Bool) {
        
        let rect = self.atackClockCircle.bounds.offsetBy(dx: -self.atackClockCircle.bounds.size.width, dy: 0.0)
        
        let circlePath = UIBezierPath(ovalIn: rect)

        self.progressCircle.path = circlePath.cgPath
        self.progressCircle.strokeColor = orange.cgColor
        self.progressCircle.fillColor = UIColor.clear.cgColor
        self.progressCircle.lineWidth = 6.0
        self.progressCircle.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.progressCircle.transform = CATransform3DMakeRotation(-(CGFloat)(M_PI_2), 0.0, 0.0, 1.0)

        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = startValue
        animation.toValue = endValue
        animation.duration = animated ? 1.0/Double(ticksInSecond) : 0.0
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        
        self.progressCircle.add(animation, forKey: "ani")
    }

    func setNormalState() {
        for case let bubbleView as MTJBballPlayerBubbleView in self.homeTeamPlayersScrollView.subviews {
            bubbleView.isHidden = false
        }
        for case let bubbleView as MTJBballPlayerBubbleView in self.awayTeamPlayersScrollView.subviews {
            bubbleView.isHidden = false
        }
        
        self.hideDarkView(animated: true)
        self.state = .normal
        self.selectedPlayerNumber = nil
    }
    
    @IBAction func tapOnAtackClock(_ sender: Any) {
        self.gameEngine.endOfAtack()
    }
    
    // MARK: - player bubble delegate
    
    func showDarkView(animated: Bool) {
        self.darkView.isHidden = false
        if self.state == .subsSelection {
            self.subsScrollView.isHidden = false
            //self.darkViewTapRecognizer.isEnabled = true
        } else if self.state == .pointsSelection {
            self.pointsView.isHidden = false
            //self.darkViewTapRecognizer.isEnabled = false
        }
        if animated == true {
            UIView.animate(withDuration: animationTime, animations: {
                self.darkView.alpha = 0.95
                if self.state == .subsSelection {
                    self.subsScrollView.alpha = 1.0
                } else if self.state == .pointsSelection {
                    self.pointsView.alpha = 1.0
                }
            })
        } else {
            self.darkView.alpha = 0.95
            if self.state == .subsSelection {
                self.subsScrollView.alpha = 1.0
            } else if self.state == .pointsSelection {
                self.pointsView.alpha = 1.0
            }
        }
    }
    
    func hideDarkView(animated: Bool) {
        if animated == true {
            UIView.animate(withDuration: animationTime, animations: {
                self.darkView.alpha = 0.0
                self.pointsView.alpha = 0.0
                self.subsScrollView.alpha = 0.0
            }, completion: { (completion: Bool) in
                self.darkView.isHidden = true
                self.pointsView.isHidden = true
                self.subsScrollView.isHidden = true
            })
        } else {
            self.darkView.alpha = 0.0
            self.pointsView.alpha = 0.0
            self.subsScrollView.alpha = 0.0
            
            self.darkView.isHidden = true
            self.pointsView.isHidden = true
            self.subsScrollView.isHidden = true
        }
    }
    
    func tapOnPlayer(_ number: Int, isHome: Bool) {
        
        switch self.state {
        case .normal:
            self.selectedPlayerNumber = number
            
            self.state = self.gameEngine.playerIsActive(with: number, isHome: isHome) == true ? .pointsSelection : .subsSelection
            if self.state == .subsSelection {
                let scrollview: UIScrollView = isHome == true ? self.homeTeamPlayersScrollView : self.awayTeamPlayersScrollView
                scrollview.scrollRectToVisible(CGRect(origin: CGPoint.zero, size: scrollview.bounds.size), animated: true)
                
                self.initSubsScrollViewBubbles(isHome: isHome)
                self.updateSubsScrollViewFrames(isHome: isHome, animated: false)
            
            } else if self.state == .pointsSelection {
                self.pointsView.prepare(number: self.selectedPlayerNumber!, isHome: isHome, delegate: self)
                
                let scrollview: UIScrollView = isHome == true ? self.homeTeamPlayersScrollView : self.awayTeamPlayersScrollView
                for case let bubbleView as MTJBballPlayerBubbleView in scrollview.subviews {
                    if bubbleView.number == self.selectedPlayerNumber {
                        bubbleView.isHidden = true
                        self.pointsView.place(at: scrollview.convert(bubbleView.center, to: self.view))
                    }
                }
            }
            self.showDarkView(animated: true)
        case .subsSelection:
            /*
            if self.gameEngine.playerIsActive(with: number, isHome: isHome) == true && self.selectedPlayerNumber != nil {
                self.gameEngine.substitutePlayer(with: number, with: self.selectedPlayerNumber!, isHome: isHome)
            }
            */
            if let newNumber = self.selectedPlayerNumber {
                self.gameEngine.substitutePlayer(with: number, with: newNumber, isHome: isHome)
            }
            self.updateScrollViewFrames(isHome: isHome, animated: true)
            self.setNormalState()
        default: break
        }
    }

    @IBAction func tapOnDarkView(_ sender: Any) {
        self.setNormalState()
    }
    
    // MARK: - points bubble delegate
    
    func tapOnScore(_ score: Int, isHome: Bool) {
        if let number = self.selectedPlayerNumber {
            self.gameEngine.playerScored(score, with: number, isHome: isHome)
            self.gameEngine.endOfAtack()
        }
        self.setNormalState()
        self.interfaceUpdate()
    }
    /*
    func tapOnClose() {
        self.setNormalState()
    }
    */
    
    @IBAction func returnFromStats(segue: UIStoryboardSegue) {
        
    }
}
