//
//  MTJSportSettingsManager.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 10/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

enum MTJBballGameTimeType: Int {
    case cleanTime = 0, dirtyTime
}

var maxPeriodsCount         = 9
var maxMinutesCount         = 99
var maxAtackSecondsCount    = 99
var maxStartPlayersCount    = 5

var minPeriodsCount         = 1
var minMinutesCount         = 1
var minAtackSecondsCount    = 1
var minStartPlayersCount    = 1

class MTJSettingsManager {
    
    var name: String = ""
    var iconName: String = ""
    
    func setDefaultValues() {
        
        if let defaultTimeType = UserDefaults.standard.object(forKey: "timeType") as! Int? {
            if let validTimeType = MTJBballGameTimeType(rawValue: defaultTimeType) {
                self.timeType = validTimeType
            }
        }
        
        if let defaultStartPlayersCount = UserDefaults.standard.object(forKey: "startPlayersCount") as! Int? {
            self.startPlayersCount = defaultStartPlayersCount
        }
        
        if let defaultPeriodsCount = UserDefaults.standard.object(forKey: "periodsCount") as! Int? {
            self.periodsCount = defaultPeriodsCount
        }
        if let defaultMinutesCount = UserDefaults.standard.object(forKey: "minutesCount") as! Int? {
            self.minutesCount = defaultMinutesCount
        }
    }
    
    func gameSettingsFinished() {
        UserDefaults.standard.synchronize()
    }
    
    func gameSettingsStarted() {
        self.setDefaultValues()
    }
    
    // MARK: Game settings
    
    var timeType: MTJBballGameTimeType = .cleanTime {
        didSet {
            UserDefaults.standard.set(timeType.rawValue, forKey:"timeType")
        }
    }
    
    var startPlayersCount: Int = 3 {
        didSet {
            if startPlayersCount > maxStartPlayersCount {
                startPlayersCount = maxStartPlayersCount
            }
            if startPlayersCount < minStartPlayersCount {
                startPlayersCount = minStartPlayersCount
            }
            UserDefaults.standard.set(startPlayersCount, forKey:"startPlayersCount")
        }
    }
    
    var canDecreaseStartPlayersCount: Bool {
        return self.startPlayersCount != minStartPlayersCount
    }
    
    var canEncreaseStartPlayersCount: Bool {
        return self.startPlayersCount != maxStartPlayersCount
    }
    
    var periodsCount: Int = 2 {
        didSet {
            if periodsCount > maxPeriodsCount {
                periodsCount = maxPeriodsCount
            }
            if periodsCount < minPeriodsCount {
                periodsCount = minPeriodsCount
            }
            UserDefaults.standard.set(periodsCount, forKey:"periodsCount")
        }
    }
    
    var canDecreasePeriodsCount: Bool {
        return self.periodsCount != minPeriodsCount
    }
    
    var canEncreasePeriodsCount: Bool {
        return self.periodsCount != maxPeriodsCount
    }
    
    var minutesCount: Int = 1 {
        didSet {
            if minutesCount > maxMinutesCount {
                minutesCount = maxMinutesCount
            }
            if minutesCount < minMinutesCount {
                minutesCount = minMinutesCount
            }
            UserDefaults.standard.set(minutesCount, forKey:"minutesCount")
        }
    }
    
    var canDecreaseMinutesCount: Bool {
        return self.minutesCount != minMinutesCount
    }
    
    var canEncreaseMinutesCount: Bool {
        return self.minutesCount != maxMinutesCount
    }
    
    // MARK: - Game teams settings
    
    var homeTeamCard: MTJTeamCard = MTJTeamCard(name: NSLocalizedString("DefaultTeamName", comment: ""))
    var awayTeamCard: MTJTeamCard = MTJTeamCard(name: NSLocalizedString("DefaultTeamName", comment: ""))
    
    var homeTeamName: String {
        get {
            return self.homeTeamCard.name
        }
        set (newName) {
            self.homeTeamCard.name = newName
        }
    }
    
    var awayTeamName: String {
        get {
            return self.awayTeamCard.name
        }
        set (newName) {
            self.awayTeamCard.name = newName
        }
    }
    
    // MARK: - Game players settings
    
    func homeTeamPlayersDescription() -> String {
        
        let keyString = "PlayersDescription"
        if self.homeTeamCard.isReady() {
            return NSLocalizedString(keyString, comment: "")
        }
        let count = self.homeTeamCard.playersNeeded()
        if count > 0 {
            return keyString.plural(for: count)
        } else {
            return NSLocalizedString(keyString, comment: "")
        }
    }
    
    func awayTeamPlayersDescription() -> String {
        
        let keyString = "PlayersDescription"
        if self.awayTeamCard.isReady() {
            return NSLocalizedString(keyString, comment: "")
        }
        let count = self.awayTeamCard.playersNeeded()
        if count > 0 {
            return keyString.plural(for: count)
        } else {
            return NSLocalizedString(keyString, comment: "")
        }
    }
    
    var homeTeamPlayersCount: Int {
        guard let players = self.homeTeamCard.players else {
            return 0
        }
        return players.count
    }
    
    var awayTeamPlayersCount: Int {
        guard let players = self.awayTeamCard.players else {
            return 0
        }
        return players.count
    }
    
    var pendingPlayerNumber: Int = 1 {
        didSet {
            if pendingPlayerNumber > maxPlayerNumber {
                pendingPlayerNumber = maxPlayerNumber
            }
            if pendingPlayerNumber < minPlayerNumber {
                pendingPlayerNumber = minPlayerNumber
            }
        }
    }
    
    var canDecreasePendingNumber: Bool {
        return self.pendingPlayerNumber != minPlayerNumber
    }
    
    var canEncreasePendingNumber: Bool {
        return self.pendingPlayerNumber != maxPlayerNumber
    }
    
    func canSetPendingNumber(number: Int) -> Bool {
        return number <= maxPlayerNumber && number >= minPlayerNumber
    }
    
    func canAddHomeTeamPlayer() -> Bool {
        return self.homeTeamCard.canUseNumber(self.pendingPlayerNumber)
    }
    
    func canAddAwayTeamPlayer() -> Bool {
        return self.awayTeamCard.canUseNumber(self.pendingPlayerNumber)
    }
    
    func addPlayerToHomeTeam(_ name: String?) {
        let player = MTJPlayerCard(number: self.pendingPlayerNumber, name: name, team:self.homeTeamCard)
        let index = self.homeTeamCard.addPlayer(player: player)
        if index != nil {
            self.selectHomeTeamPlayer(at: index!, isSelected: true)
        }
    }
    
    func addPlayerToAwayTeam(_ name: String?) {
        let player = MTJPlayerCard(number: self.pendingPlayerNumber, name: name, team:self.awayTeamCard)
        let index = self.awayTeamCard.addPlayer(player: player)
        if index != nil {
            self.selectAwayTeamPlayer(at: index!, isSelected: true)
        }
    }
    
    func removePlayerFromHomeTeam(at index: Int) {
        self.homeTeamCard.removePlayer(at: index)
    }
    
    func removePlayerFromAwayTeam(at index: Int) {
        self.awayTeamCard.removePlayer(at: index)
    }
    
    func moveHomeTeamPlayer(from fromIndex: Int, to toIndex: Int) {
        self.homeTeamCard.movePlayer(from: fromIndex, to: toIndex)
    }
    
    func moveAwayTeamPlayer(from fromIndex: Int, to toIndex: Int) {
        self.awayTeamCard.movePlayer(from: fromIndex, to: toIndex)
    }
    
    func homeTeamPlayerIsSelected(at index: Int) -> Bool {
        return self.homeTeamCard.playerIsSelected(at: index)
    }
    
    func awayTeamPlayerIsSelected(at index: Int) -> Bool {
        return self.awayTeamCard.playerIsSelected(at: index)
    }
    
    func playersName(at index: Int, isHome: Bool) -> String? {
        if isHome == true {
            return self.homeTeamCard.playersName(at: index)
        } else {
            return self.awayTeamCard.playersName(at: index)
        }
    }
    
    func playersName(with number: Int, isHome: Bool) -> String? {
        if isHome == true {
            return self.homeTeamCard.playersName(with: number)
        } else {
            return self.awayTeamCard.playersName(with: number)
        }
    }
    
    func homeTeamPlayersNumber(at index: Int) -> Int? {
        return self.homeTeamCard.playersNumber(at: index)
    }
    
    func awayTeamPlayersNumber(at index: Int) -> Int? {
        return self.awayTeamCard.playersNumber(at: index)
    }
    
    func homeTeamPlayerCanBeSelected() -> Bool {
        return self.homeTeamCard.playerCanBeSelected()
    }
    
    func awayTeamPlayerCanBeSelected() -> Bool {
        return self.awayTeamCard.playerCanBeSelected()
    }
    
    func selectHomeTeamPlayer(at index: Int, isSelected: Bool) {
        self.homeTeamCard.selectPlayer(at: index, isSelected: isSelected)
    }
    
    func selectAwayTeamPlayer(at index: Int, isSelected: Bool) {
        self.awayTeamCard.selectPlayer(at: index, isSelected: isSelected)
    }
    
    var isReady: Bool {
        return self.homeTeamCard.isReady() && self.awayTeamCard.isReady()
    }
}
