//
//  String+Plurals.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 03/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import Foundation

extension String {
    func plural(for qty: Int) -> String {
        if qty % 10 == 1 && qty % 100 != 11 {
            return String.localizedStringWithFormat(NSLocalizedString("one \(self)", comment: ""), qty)
        } else if 2...4 ~= qty % 10 && !(12...14 ~= qty % 100) {
            return String.localizedStringWithFormat(NSLocalizedString("few \(self)", comment: ""), qty)
        } else {
            return String.localizedStringWithFormat(NSLocalizedString("other \(self)", comment: ""), qty)
        }
    }
}
