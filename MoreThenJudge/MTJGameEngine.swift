//
//  MTJSportGameEngine.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 10/02/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

let ticksInSecond = 10

enum MTJBasketballGameState {
    case inPlay, onPause, isEnded
}

import Foundation

class MTJGameEngine {
    
    let basketballManager = MTJBasketballSettingsManager.basketballManager
    
    let homeTeam: MTJTeam
    let awayTeam: MTJTeam
    
    init?(homeTeamCard: MTJTeamCard, awayTeamCard: MTJTeamCard) {
        guard let homePlayers = homeTeamCard.players else {
            return nil
        }
        var homeTeamPlayers = [MTJPlayer]()
        for playerCard in homePlayers {
            homeTeamPlayers.append(MTJPlayer(playerCard.number, isActive: playerCard.isSelected))
        }
        self.homeTeam = MTJTeam(homeTeamPlayers)
        
        guard let awayPlayers = awayTeamCard.players else {
            return nil
        }
        var awayTeamPlayers = [MTJPlayer]()
        for playerCard in awayPlayers {
            awayTeamPlayers.append(MTJPlayer(playerCard.number, isActive: playerCard.isSelected))
        }
        self.awayTeam = MTJTeam(awayTeamPlayers)
    }
    
    var homeTeamName: String {
        return basketballManager.homeTeamName
    }
    var awayTeamName: String {
        return basketballManager.awayTeamName
    }
    
    var homeTeamPlayersCount: Int {
        return self.homeTeam.players.count
    }
    var awayTeamPlayersCount: Int {
        return self.awayTeam.players.count
    }
    
    var activePlayersCount: Int {
        return basketballManager.startPlayersCount
    }
    
    func playerNumber(at index: Int, isHome: Bool) -> Int {
        if isHome == true {
            return self.homeTeam.players[index].number
        } else {
            return self.awayTeam.players[index].number
        }
    }
    
    func playerName(at index: Int, isHome: Bool) -> String? {
        return basketballManager.playersName(at: index, isHome: isHome)
    }
    
    func playerName(with number: Int, isHome: Bool) -> String? {
        return basketballManager.playersName(with: number, isHome: isHome)
    }
    
    func activePlayerNumber(at index: Int, isHome: Bool) -> Int? {
        if isHome == true {
            return self.homeTeam.activePlayerNumber(at: index)
        } else {
            return self.awayTeam.activePlayerNumber(at: index)
        }
    }
    
    func activePlayerName(at index: Int, isHome: Bool) -> String? {
        if isHome == true {
            if let number = self.homeTeam.activePlayerNumber(at: index) {
                return basketballManager.playersName(with: number, isHome: isHome)
            }
        } else {
            if let number = self.awayTeam.activePlayerNumber(at: index) {
                return basketballManager.playersName(with: number, isHome: isHome)
            }
        }
        return nil
    }
    
    func playerIsActive(at index: Int, isHome: Bool) -> Bool {
        if isHome == true {
            return self.homeTeam.players[index].isActive
        } else {
            return self.awayTeam.players[index].isActive
        }
    }
    
    func indexOfPlayer(with number: Int, isHome: Bool) -> Int? {
        if isHome == true {
            return self.homeTeam.indexOfPlayer(with: number)
        } else {
            return self.awayTeam.indexOfPlayer(with: number)
        }
    }
    
    func indexOfActivePlayer(with number: Int, isHome: Bool) -> Int? {
        if isHome == true {
            return self.homeTeam.indexOfActivePlayer(with: number)
        } else {
            return self.awayTeam.indexOfActivePlayer(with: number)
        }
    }
    
    func playerIsActive(with number: Int, isHome: Bool) -> Bool? {
        if isHome == true {
            return self.homeTeam.player(with: number)?.isActive
        } else {
            return self.awayTeam.player(with: number)?.isActive
        }
    }
    
    func substitutePlayer(at index: Int, with newIndex: Int, isHome: Bool) {
        if isHome == true {
            self.homeTeam.substitutePlayer(at: index, with: newIndex)
        } else {
            self.awayTeam.substitutePlayer(at: index, with: newIndex)
        }
    }
    
    func substitutePlayer(with number: Int, with newNumber: Int, isHome: Bool) {
        if isHome == true {
            self.homeTeam.substitutePlayer(with: number, with: newNumber)
        } else {
            self.awayTeam.substitutePlayer(with: number, with: newNumber)
        }
    }
    
    func playerScored(_ score: Int, with number: Int, isHome: Bool) {
        if isHome == true {
            self.homeTeam.playerScored(score, with: number)
        } else {
            self.awayTeam.playerScored(score, with: number)
        }
    }
    
    var state = MTJBasketballGameState.onPause
    
    var timer: Timer?
    
    private var currentPeriodTimeLeft: Int = 0 {
        didSet {
            if currentPeriodTimeLeft <= 0 {
                if currentPeriodNumber >= basketballManager.periodsCount {
                    self.endOfGame()
                } else {
                    self.endOfPeriod()
                }
            }
        }
    }
    
    var currentPeriodSecondsLeft: Int {
        return self.currentPeriodTimeLeft / ticksInSecond + (self.currentPeriodTimeLeft % ticksInSecond > 0 ? 1 : 0)
    }
    
    var currentPeriodNumber: Int = 0 {
        didSet {
            if currentPeriodNumber > basketballManager.periodsCount {
                self.endOfGame()
            }
        }
    }
    
    var currentHomeTeamScore: Int {
        return self.homeTeam.score
    }
    var currentAwayTeamScore: Int {
        return self.awayTeam.score
    }
    
    func decreaseCurrentPeriodTimeLeft() {
        self.currentPeriodTimeLeft -= 1
    }
    
    func resetCurrentPeriodTimeLeft(isOvertime: Bool) {
        let minutes = isOvertime == true ? basketballManager.overtimeMinutesCount : basketballManager.minutesCount
        self.currentPeriodTimeLeft = minutes * 60 * ticksInSecond
    }
    
    func encreaseCurrentPeriodNumber() {
        self.currentPeriodNumber += 1
    }
    
    func resetCurrentPeriodNumber() {
        self.currentPeriodNumber = 1
    }
    
    var currentPeriodString: String {
        return self.currentPeriodNumber > basketballManager.periodsCount ? NSLocalizedString("Overtime", comment: "") : "\(self.currentPeriodNumber)"
    }
    
    var isInPlay: Bool {
        return self.state == .inPlay
    }
    
    var isOnPause: Bool {
        return self.state == .onPause
    }
    
    var isEnded: Bool {
        return self.state == .isEnded
    }

    func startGame() {
        guard self.state == .onPause else {
            return
        }
        self.resetCurrentPeriodNumber()
        self.resetCurrentPeriodTimeLeft(isOvertime: false)
        self.resumeGame()
    }
    
    func resumeGame() {
        guard self.state == .onPause else {
            return
        }
        self.state = .inPlay
        self.runTimer()
        NotificationCenter.default.post(name: Notification.Name(notificationNameGameInterfaceUpdate), object: nil)
        NotificationCenter.default.post(name: Notification.Name(notificationNameGameStartAtackClockAnimation), object: nil)
    }
    
    func stopGame() {
        guard self.state == .inPlay else {
            return
        }
        self.pauseGame()
        self.state = .isEnded
        NotificationCenter.default.post(name: Notification.Name(notificationNameGameEnded), object: nil)
    }
    
    func pauseGame() {
        guard self.state == .inPlay else {
            return
        }
        self.state = .onPause
        self.pauseTimer()
        NotificationCenter.default.post(name: Notification.Name(notificationNameGameInterfaceUpdate), object: nil)
        NotificationCenter.default.post(name: Notification.Name(notificationNameGamePauseAtackClockAnimation), object: nil)
    }
    
    func runTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1.0/Double(ticksInSecond), target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
    }
    
    func pauseTimer() {
        self.timer?.invalidate()
    }
    
    func endOfPeriod() {
        
        self.resetCurrentPeriodTimeLeft(isOvertime: false)
        self.encreaseCurrentPeriodNumber()
    }
    
    func endOfGame() {
        
        if self.currentHomeTeamScore == self.currentAwayTeamScore {
            self.resetCurrentPeriodTimeLeft(isOvertime: true)
        } else {
            self.stopGame()
        }
    }
    
    @objc func timerTick() {
        self.decreaseCurrentPeriodTimeLeft()
        NotificationCenter.default.post(name: Notification.Name(notificationNameGameStartAtackClockAnimation), object: nil)
        NotificationCenter.default.post(name: Notification.Name(notificationNameGameInterfaceUpdate), object: nil)
    }
    
    func playersScore(with number: Int, isHome: Bool) -> Int {
        if isHome == true {
            return self.homeTeam.playersScore(with: number)
        } else {
            return self.awayTeam.playersScore(with: number)
        }
    }
}
