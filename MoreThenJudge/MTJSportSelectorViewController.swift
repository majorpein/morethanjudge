//
//  ViewController.swift
//  MoreThenJudge
//
//  Created by Alexander Anosov on 23/01/2017.
//  Copyright © 2017 RognarDev. All rights reserved.
//

import UIKit

protocol MTJSportInitialProtocol {
    func name() -> String
    func iconName() -> String
}

class MTJSportSelectorViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var titleLabel: UILabel!
    
    let sports = [
        "MTJBballGameSettingsTableViewController",
        "MTJBballGameSettingsTableViewController"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.prepareScrollViewContent()
        
        self.startButton.layer.shadowOpacity = 0.24
        self.startButton.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func prepareScrollViewContent() {
        
        self.scrollView.clipsToBounds = false
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(self.sports.count), height: self.scrollView.frame.size.height)
        
        var sportsCount = 0
        
        for (index, identifier) in self.sports.enumerated() {
            guard let initialController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier) as? MTJSportInitialProtocol else {
                continue
            }
            
            let imageView = UIImageView(image: UIImage(named: initialController.iconName()))
            imageView.contentMode = .scaleAspectFit
            imageView.frame = CGRect(x: self.scrollView.frame.size.width * CGFloat(index), y: 0.0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height * 1.2)
            self.scrollView.addSubview(imageView)
            
            sportsCount += 1
        }
        
        self.pageControl.numberOfPages = sportsCount
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageControl.currentPage = Int(pageNumber)
    }
    
    @IBAction func pageChanged(_ sender: Any) {
        let x = CGFloat(self.pageControl.currentPage) * self.scrollView.frame.size.width
        self.scrollView.setContentOffset(CGPoint(x: x,y :0), animated: true)
    }
    
    @IBAction func onStartClicked(_ sender: Any) {
        
    }

}

